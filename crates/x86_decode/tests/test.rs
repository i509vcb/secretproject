use x86_decode::{DecodeError, Decoder, DecoderTables, FetchBuffer, OpcodeOptions, OpcodeTables};
use x86_types::{CodeSize, Instruction, Isa, Memory, Opcode, Operands, Reg8, Segment};

#[derive(Default)]
struct Builder {
    isa: Option<Isa>,
    undefined: bool,
    mode: CodeSize,
}

impl Builder {
    pub fn isa(mut self, isa: Isa) -> Self {
        self.isa = Some(isa);
        self
    }

    pub fn undefined(mut self) -> Self {
        self.undefined = true;
        self
    }

    pub fn mode(mut self, address_size: CodeSize) -> Self {
        self.mode = address_size;
        self
    }

    pub fn decode(self, bytes: &[u8]) -> Result<(Instruction, u16), DecodeError> {
        let options = OpcodeOptions {
            isa: self.isa.unwrap_or(Isa::_8086),
            allow_undefined: self.undefined,
        };

        let byte_1 = OpcodeTables::byte_1_table(options);
        let byte_2 = if options.isa.is_386() {
            Some(OpcodeTables::byte_2_table(options))
        } else {
            None
        };

        let tables = DecoderTables {
            byte_1: &byte_1,
            byte_2: byte_2.as_ref(),
        };

        let mut buffer = FetchBuffer::with_slice(bytes);

        let decoder = Decoder { max_size: 15 };

        let inst = decoder.read_instruction(&tables, &mut buffer, self.mode).unwrap();

        Ok((inst, buffer.len()))
    }
}

#[test]
fn dec_mem() {
    let (inst, len) = Builder::default().decode(&[0xFE, 0x08]).unwrap();

    assert!(!inst.lock);
    assert_eq!(inst.opcode, Opcode::Dec);
    assert_eq!(inst.operands, Operands::Memory8(Segment::Ds, Memory::BxSi(0)));
    assert_eq!(len, 2);
}

#[test]
fn inc_mem() {
    let (inst, len) = Builder::default().decode(&[0xFE, 0x00]).unwrap();

    assert!(!inst.lock);
    assert_eq!(inst.opcode, Opcode::Inc);
    assert_eq!(inst.operands, Operands::Memory8(Segment::Ds, Memory::BxSi(0)));
    assert_eq!(len, 2);
}

#[test]
fn xchg_r8_r8() {
    let (inst, len) = Builder::default().decode(&[0x86, 0xC0]).unwrap();

    assert!(!inst.lock);
    assert_eq!(inst.opcode, Opcode::Xchg);
    assert_eq!(inst.operands, Operands::RegisterRegister8(Reg8::Al, Reg8::Al));
    assert_eq!(len, 2);
}

#[test]
fn xchg_m8_r8() {
    let (inst, len) = Builder::default().decode(&[0x86, 0x00]).unwrap();

    assert!(!inst.lock);
    assert_eq!(inst.opcode, Opcode::Xchg);
    assert_eq!(
        inst.operands,
        Operands::MemoryRegister8(Segment::Ds, Memory::BxSi(0), Reg8::Al)
    );
    assert_eq!(len, 2);
}

#[test]
fn xchg_m8_r8_o32_a16() {
    let (inst, len) = Builder::default()
        .mode(CodeSize::_32)
        .isa(Isa::_386)
        .decode(&[0x67, 0x86, 0x00])
        .unwrap();

    assert!(!inst.lock);
    assert_eq!(inst.opcode, Opcode::Xchg);
    assert_eq!(
        inst.operands,
        Operands::MemoryRegister8(Segment::Ds, Memory::BxSi(0), Reg8::Al)
    );
    assert_eq!(len, 3);

    let (inst, len) = Builder::default()
        .mode(CodeSize::_32)
        .isa(Isa::_386)
        .decode(&[0x67, 0x86, 0x01])
        .unwrap();

    assert!(!inst.lock);
    assert_eq!(inst.opcode, Opcode::Xchg);
    assert_eq!(
        inst.operands,
        Operands::MemoryRegister8(Segment::Ds, Memory::BxDi(0), Reg8::Al)
    );
    assert_eq!(len, 3);
}

#[test]
fn add_m8_r8_o16_a16() {
    let (inst, len) = Builder::default().decode(&[0x0, 0x00]).unwrap();

    assert!(!inst.lock);
    assert_eq!(inst.opcode, Opcode::Add);
    assert_eq!(
        inst.operands,
        Operands::MemoryRegister8(Segment::Ds, Memory::BxSi(0), Reg8::Al)
    );
    assert_eq!(len, 2);
}

#[test]
fn add_r8_r8_o16_a16() {
    let (inst, len) = Builder::default().decode(&[0x0, 0xC0]).unwrap();

    assert!(!inst.lock);
    assert_eq!(inst.opcode, Opcode::Add);
    assert_eq!(inst.operands, Operands::RegisterRegister8(Reg8::Al, Reg8::Al));
    assert_eq!(len, 2);
}

#[test]
fn jmp_ptr1616() {
    let (inst, len) = Builder::default().decode(&[0xEA, 0x00, 0x00, 0x80, 0xFD]).unwrap();

    assert!(!inst.lock);
    assert_eq!(inst.opcode, Opcode::Jmp);
    assert_eq!(
        inst.operands,
        Operands::DirectAddress16 {
            segment: 0xFD80,
            displacement: 0x0000
        }
    );
    assert_eq!(len, 5);
}
