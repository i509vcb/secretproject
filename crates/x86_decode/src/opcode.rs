//! Opcode tables

use enumset::{enum_set, EnumSet};
use x86_types::Isa;

use crate::{Attrs, Group, ModrmMask, Opcode, OpcodeInfo, OpcodeOptions, Signature, Ty};

// TODO: Add modrm to table.
pub const fn byte_1(options: OpcodeOptions) -> [OpcodeInfo; 256] {
    let mut info = [OpcodeInfo(Ty::None, enum_set!()); 256];
    // 0x00
    info[0x00] = OpcodeInfo(
        Ty::Op(Opcode::Add, Signature::Eb_Gb, ModrmMask::empty()),
        Attrs::ALU_MODRM_LOCK,
    );
    info[0x01] = OpcodeInfo(
        Ty::Op(Opcode::Add, Signature::Ev_Gv, ModrmMask::empty()),
        Attrs::ALU_MODRM_LOCK,
    );
    info[0x02] = OpcodeInfo(
        Ty::Op(Opcode::Add, Signature::Gb_Eb, ModrmMask::empty()),
        Attrs::ALU_MODRM,
    );
    info[0x03] = OpcodeInfo(
        Ty::Op(Opcode::Add, Signature::Gv_Ev, ModrmMask::empty()),
        Attrs::ALU_MODRM,
    );
    info[0x04] = OpcodeInfo(Ty::Op(Opcode::Add, Signature::Al_Ib, ModrmMask::empty()), Attrs::ALU);
    info[0x05] = OpcodeInfo(Ty::Op(Opcode::Add, Signature::eAx_Iv, ModrmMask::empty()), Attrs::ALU);
    info[0x06] = OpcodeInfo(Ty::Op(Opcode::Push, Signature::Es, ModrmMask::empty()), enum_set!());
    info[0x07] = OpcodeInfo(Ty::Op(Opcode::Pop, Signature::Es, ModrmMask::empty()), enum_set!());

    // 0x08
    info[0x08] = OpcodeInfo(
        Ty::Op(Opcode::Or, Signature::Eb_Gb, ModrmMask::empty()),
        Attrs::BIT_MODRM_LOCK,
    );
    info[0x09] = OpcodeInfo(
        Ty::Op(Opcode::Or, Signature::Ev_Gv, ModrmMask::empty()),
        Attrs::BIT_MODRM_LOCK,
    );
    info[0x0A] = OpcodeInfo(
        Ty::Op(Opcode::Or, Signature::Gb_Eb, ModrmMask::empty()),
        Attrs::BIT_MODRM,
    );
    info[0x0B] = OpcodeInfo(
        Ty::Op(Opcode::Or, Signature::Gv_Ev, ModrmMask::empty()),
        Attrs::BIT_MODRM,
    );
    info[0x0C] = OpcodeInfo(Ty::Op(Opcode::Or, Signature::Al_Ib, ModrmMask::empty()), Attrs::BIT);
    info[0x0D] = OpcodeInfo(Ty::Op(Opcode::Or, Signature::eAx_Iv, ModrmMask::empty()), Attrs::BIT);
    info[0x0E] = OpcodeInfo(Ty::Op(Opcode::Push, Signature::Cs, ModrmMask::empty()), enum_set!());
    // FIXME: Does V30 have Pop CS?
    if matches!(options.isa, Isa::_8086 | Isa::V30) && options.allow_undefined {
        info[0x0F] = OpcodeInfo(
            Ty::Op(Opcode::Pop, Signature::Cs, ModrmMask::empty()),
            enum_set!(Attrs::Undefined),
        );
    } else if options.isa.is_286() {
        info[0x0F] = OpcodeInfo(Ty::Prefix, enum_set!());
    }

    // 0x10
    info[0x10] = OpcodeInfo(
        Ty::Op(Opcode::Adc, Signature::Eb_Gb, ModrmMask::empty()),
        Attrs::ALU_BORROW_MODRM_LOCK,
    );
    info[0x11] = OpcodeInfo(
        Ty::Op(Opcode::Adc, Signature::Ev_Gv, ModrmMask::empty()),
        Attrs::ALU_BORROW_MODRM_LOCK,
    );
    info[0x12] = OpcodeInfo(
        Ty::Op(Opcode::Adc, Signature::Gb_Eb, ModrmMask::empty()),
        Attrs::ALU_BORROW_MODRM,
    );
    info[0x13] = OpcodeInfo(
        Ty::Op(Opcode::Adc, Signature::Gv_Ev, ModrmMask::empty()),
        Attrs::ALU_BORROW_MODRM,
    );
    info[0x14] = OpcodeInfo(
        Ty::Op(Opcode::Adc, Signature::Al_Ib, ModrmMask::empty()),
        Attrs::ALU_BORROW,
    );
    info[0x15] = OpcodeInfo(
        Ty::Op(Opcode::Adc, Signature::eAx_Iv, ModrmMask::empty()),
        Attrs::ALU_BORROW,
    );
    info[0x16] = OpcodeInfo(Ty::Op(Opcode::Push, Signature::Ss, ModrmMask::empty()), enum_set!());
    info[0x17] = OpcodeInfo(Ty::Op(Opcode::Pop, Signature::Ss, ModrmMask::empty()), enum_set!());

    // 0x18
    info[0x18] = OpcodeInfo(
        Ty::Op(Opcode::Sbb, Signature::Eb_Gb, ModrmMask::empty()),
        Attrs::ALU_BORROW_MODRM_LOCK,
    );
    info[0x19] = OpcodeInfo(
        Ty::Op(Opcode::Sbb, Signature::Ev_Gv, ModrmMask::empty()),
        Attrs::ALU_BORROW_MODRM_LOCK,
    );
    info[0x1A] = OpcodeInfo(
        Ty::Op(Opcode::Sbb, Signature::Gb_Eb, ModrmMask::empty()),
        Attrs::ALU_BORROW_MODRM,
    );
    info[0x1B] = OpcodeInfo(
        Ty::Op(Opcode::Sbb, Signature::Gv_Ev, ModrmMask::empty()),
        Attrs::ALU_BORROW_MODRM,
    );
    info[0x1C] = OpcodeInfo(
        Ty::Op(Opcode::Sbb, Signature::Al_Ib, ModrmMask::empty()),
        Attrs::ALU_BORROW,
    );
    info[0x1D] = OpcodeInfo(
        Ty::Op(Opcode::Sbb, Signature::eAx_Iv, ModrmMask::empty()),
        Attrs::ALU_BORROW,
    );
    info[0x1E] = OpcodeInfo(Ty::Op(Opcode::Push, Signature::Ds, ModrmMask::empty()), enum_set!());
    info[0x1F] = OpcodeInfo(Ty::Op(Opcode::Pop, Signature::Ds, ModrmMask::empty()), enum_set!());

    // 0x20
    info[0x20] = OpcodeInfo(
        Ty::Op(Opcode::And, Signature::Eb_Gb, ModrmMask::empty()),
        Attrs::BIT_MODRM_LOCK,
    );
    info[0x21] = OpcodeInfo(
        Ty::Op(Opcode::And, Signature::Ev_Gv, ModrmMask::empty()),
        Attrs::BIT_MODRM_LOCK,
    );
    info[0x22] = OpcodeInfo(
        Ty::Op(Opcode::And, Signature::Gb_Eb, ModrmMask::empty()),
        Attrs::BIT_MODRM,
    );
    info[0x23] = OpcodeInfo(
        Ty::Op(Opcode::And, Signature::Gv_Ev, ModrmMask::empty()),
        Attrs::BIT_MODRM,
    );
    info[0x24] = OpcodeInfo(Ty::Op(Opcode::And, Signature::Al_Ib, ModrmMask::empty()), Attrs::BIT);
    info[0x25] = OpcodeInfo(Ty::Op(Opcode::And, Signature::eAx_Iv, ModrmMask::empty()), Attrs::BIT);
    info[0x26] = OpcodeInfo(Ty::Prefix, enum_set!());
    info[0x27] = OpcodeInfo(
        Ty::Op(Opcode::Daa, Signature::Empty, ModrmMask::empty()),
        Attrs::BCD_ADJUST,
    );

    // 0x28
    info[0x28] = OpcodeInfo(
        Ty::Op(Opcode::Sub, Signature::Eb_Gb, ModrmMask::empty()),
        Attrs::ALU_MODRM_LOCK,
    );
    info[0x29] = OpcodeInfo(
        Ty::Op(Opcode::Sub, Signature::Ev_Gv, ModrmMask::empty()),
        Attrs::ALU_MODRM_LOCK,
    );
    info[0x2A] = OpcodeInfo(
        Ty::Op(Opcode::Sub, Signature::Gb_Eb, ModrmMask::empty()),
        Attrs::ALU_MODRM,
    );
    info[0x2B] = OpcodeInfo(
        Ty::Op(Opcode::Sub, Signature::Gv_Ev, ModrmMask::empty()),
        Attrs::ALU_MODRM,
    );
    info[0x2C] = OpcodeInfo(Ty::Op(Opcode::Sub, Signature::Al_Ib, ModrmMask::empty()), Attrs::ALU);
    info[0x2D] = OpcodeInfo(Ty::Op(Opcode::Sub, Signature::eAx_Iv, ModrmMask::empty()), Attrs::ALU);
    info[0x2E] = OpcodeInfo(Ty::Prefix, enum_set!());
    info[0x2F] = OpcodeInfo(
        Ty::Op(Opcode::Das, Signature::Empty, ModrmMask::empty()),
        Attrs::BCD_ADJUST,
    );

    // 0x30
    info[0x30] = OpcodeInfo(
        Ty::Op(Opcode::Xor, Signature::Eb_Gb, ModrmMask::empty()),
        Attrs::BIT_MODRM_LOCK,
    );
    info[0x31] = OpcodeInfo(
        Ty::Op(Opcode::Xor, Signature::Ev_Gv, ModrmMask::empty()),
        Attrs::BIT_MODRM_LOCK,
    );
    info[0x32] = OpcodeInfo(
        Ty::Op(Opcode::Xor, Signature::Gb_Eb, ModrmMask::empty()),
        Attrs::BIT_MODRM,
    );
    info[0x33] = OpcodeInfo(
        Ty::Op(Opcode::Xor, Signature::Gv_Ev, ModrmMask::empty()),
        Attrs::BIT_MODRM,
    );
    info[0x34] = OpcodeInfo(Ty::Op(Opcode::Xor, Signature::Al_Ib, ModrmMask::empty()), Attrs::BIT);
    info[0x35] = OpcodeInfo(Ty::Op(Opcode::Xor, Signature::eAx_Iv, ModrmMask::empty()), Attrs::BIT);
    info[0x36] = OpcodeInfo(Ty::Prefix, enum_set!());
    info[0x37] = OpcodeInfo(
        Ty::Op(Opcode::Aaa, Signature::Empty, ModrmMask::empty()),
        Attrs::ASCII_ADJUST,
    );

    // 0x38
    info[0x38] = OpcodeInfo(
        Ty::Op(Opcode::Cmp, Signature::Eb_Gb, ModrmMask::empty()),
        Attrs::ALU_MODRM_LOCK,
    );
    info[0x39] = OpcodeInfo(
        Ty::Op(Opcode::Cmp, Signature::Ev_Gv, ModrmMask::empty()),
        Attrs::ALU_MODRM_LOCK,
    );
    info[0x3A] = OpcodeInfo(
        Ty::Op(Opcode::Cmp, Signature::Gb_Eb, ModrmMask::empty()),
        Attrs::ALU_MODRM,
    );
    info[0x3B] = OpcodeInfo(
        Ty::Op(Opcode::Cmp, Signature::Gv_Ev, ModrmMask::empty()),
        Attrs::ALU_MODRM,
    );
    info[0x3C] = OpcodeInfo(Ty::Op(Opcode::Cmp, Signature::Al_Ib, ModrmMask::empty()), Attrs::ALU);
    info[0x3D] = OpcodeInfo(Ty::Op(Opcode::Cmp, Signature::eAx_Iv, ModrmMask::empty()), Attrs::ALU);
    info[0x3E] = OpcodeInfo(Ty::Prefix, enum_set!());
    info[0x3F] = OpcodeInfo(
        Ty::Op(Opcode::Aas, Signature::Empty, ModrmMask::empty()),
        Attrs::ASCII_ADJUST,
    );

    // 0x40
    info[0x40] = OpcodeInfo(Ty::Op(Opcode::Inc, Signature::eAx, ModrmMask::empty()), Attrs::INC_DEC);
    info[0x41] = OpcodeInfo(Ty::Op(Opcode::Inc, Signature::eCx, ModrmMask::empty()), Attrs::INC_DEC);
    info[0x42] = OpcodeInfo(Ty::Op(Opcode::Inc, Signature::eDx, ModrmMask::empty()), Attrs::INC_DEC);
    info[0x43] = OpcodeInfo(Ty::Op(Opcode::Inc, Signature::eBx, ModrmMask::empty()), Attrs::INC_DEC);
    info[0x44] = OpcodeInfo(Ty::Op(Opcode::Inc, Signature::eSp, ModrmMask::empty()), Attrs::INC_DEC);
    info[0x45] = OpcodeInfo(Ty::Op(Opcode::Inc, Signature::eBp, ModrmMask::empty()), Attrs::INC_DEC);
    info[0x46] = OpcodeInfo(Ty::Op(Opcode::Inc, Signature::eSi, ModrmMask::empty()), Attrs::INC_DEC);
    info[0x47] = OpcodeInfo(Ty::Op(Opcode::Inc, Signature::eDi, ModrmMask::empty()), Attrs::INC_DEC);

    // 0x48
    info[0x48] = OpcodeInfo(Ty::Op(Opcode::Dec, Signature::eAx, ModrmMask::empty()), Attrs::INC_DEC);
    info[0x49] = OpcodeInfo(Ty::Op(Opcode::Dec, Signature::eCx, ModrmMask::empty()), Attrs::INC_DEC);
    info[0x4A] = OpcodeInfo(Ty::Op(Opcode::Dec, Signature::eDx, ModrmMask::empty()), Attrs::INC_DEC);
    info[0x4B] = OpcodeInfo(Ty::Op(Opcode::Dec, Signature::eBx, ModrmMask::empty()), Attrs::INC_DEC);
    info[0x4C] = OpcodeInfo(Ty::Op(Opcode::Dec, Signature::eSp, ModrmMask::empty()), Attrs::INC_DEC);
    info[0x4D] = OpcodeInfo(Ty::Op(Opcode::Dec, Signature::eBp, ModrmMask::empty()), Attrs::INC_DEC);
    info[0x4E] = OpcodeInfo(Ty::Op(Opcode::Dec, Signature::eSi, ModrmMask::empty()), Attrs::INC_DEC);
    info[0x4F] = OpcodeInfo(Ty::Op(Opcode::Dec, Signature::eDi, ModrmMask::empty()), Attrs::INC_DEC);

    // 0x50
    info[0x50] = OpcodeInfo(Ty::Op(Opcode::Push, Signature::eAx, ModrmMask::empty()), enum_set!());
    info[0x51] = OpcodeInfo(Ty::Op(Opcode::Push, Signature::eCx, ModrmMask::empty()), enum_set!());
    info[0x52] = OpcodeInfo(Ty::Op(Opcode::Push, Signature::eDx, ModrmMask::empty()), enum_set!());
    info[0x53] = OpcodeInfo(Ty::Op(Opcode::Push, Signature::eBx, ModrmMask::empty()), enum_set!());
    info[0x54] = OpcodeInfo(Ty::Op(Opcode::Push, Signature::eSp, ModrmMask::empty()), enum_set!());
    info[0x55] = OpcodeInfo(Ty::Op(Opcode::Push, Signature::eBp, ModrmMask::empty()), enum_set!());
    info[0x56] = OpcodeInfo(Ty::Op(Opcode::Push, Signature::eSi, ModrmMask::empty()), enum_set!());
    info[0x57] = OpcodeInfo(Ty::Op(Opcode::Push, Signature::eDi, ModrmMask::empty()), enum_set!());

    // 0x58
    info[0x58] = OpcodeInfo(Ty::Op(Opcode::Pop, Signature::eAx, ModrmMask::empty()), enum_set!());
    info[0x59] = OpcodeInfo(Ty::Op(Opcode::Pop, Signature::eCx, ModrmMask::empty()), enum_set!());
    info[0x5A] = OpcodeInfo(Ty::Op(Opcode::Pop, Signature::eDx, ModrmMask::empty()), enum_set!());
    info[0x5B] = OpcodeInfo(Ty::Op(Opcode::Pop, Signature::eBx, ModrmMask::empty()), enum_set!());
    info[0x5C] = OpcodeInfo(Ty::Op(Opcode::Pop, Signature::eSp, ModrmMask::empty()), enum_set!());
    info[0x5D] = OpcodeInfo(Ty::Op(Opcode::Pop, Signature::eBp, ModrmMask::empty()), enum_set!());
    info[0x5E] = OpcodeInfo(Ty::Op(Opcode::Pop, Signature::eSi, ModrmMask::empty()), enum_set!());
    info[0x5F] = OpcodeInfo(Ty::Op(Opcode::Pop, Signature::eDi, ModrmMask::empty()), enum_set!());

    // 0x60
    if options.isa.is_186() {
        info[0x60] = OpcodeInfo(Ty::Op(Opcode::Pusha, Signature::Empty, ModrmMask::empty()), enum_set!());
        info[0x61] = OpcodeInfo(Ty::Op(Opcode::Popa, Signature::Empty, ModrmMask::empty()), enum_set!());
        info[0x62] = OpcodeInfo(Ty::Op(Opcode::Bound, Signature::Gv_Ma, ModrmMask::empty()), enum_set!());
    }
    if options.isa.is_286() {
        info[0x63] = OpcodeInfo(
            Ty::Op(Opcode::Arpl, Signature::Ew_Rw, ModrmMask::empty()),
            enum_set!(Attrs::UseModrm),
        );
    }
    if options.isa.is_386() {
        info[0x64] = OpcodeInfo(Ty::Prefix, enum_set!());
        info[0x65] = OpcodeInfo(Ty::Prefix, enum_set!());
        info[0x66] = OpcodeInfo(Ty::Prefix, enum_set!());
        info[0x67] = OpcodeInfo(Ty::Prefix, enum_set!());
    }

    // 0x68
    if options.isa.is_186() {
        info[0x68] = OpcodeInfo(Ty::Op(Opcode::Push, Signature::Iv, ModrmMask::empty()), enum_set!());
        info[0x69] = OpcodeInfo(
            Ty::Op(Opcode::Imul, Signature::Gv_Ev_Iv, ModrmMask::empty()),
            Attrs::IMUL,
        );
        info[0x6A] = OpcodeInfo(Ty::Op(Opcode::Push, Signature::Ib, ModrmMask::empty()), enum_set!());
        info[0x6B] = OpcodeInfo(
            Ty::Op(Opcode::Imul, Signature::Gv_Ev_IbExtend, ModrmMask::empty()),
            Attrs::IMUL,
        );
        info[0x6C] = OpcodeInfo(
            Ty::Op(Opcode::Ins, Signature::Yb_Dx, ModrmMask::empty()),
            enum_set!(Attrs::RepRepe),
        );
        info[0x6D] = OpcodeInfo(
            Ty::Op(Opcode::Ins, Signature::Yv_Dx, ModrmMask::empty()),
            enum_set!(Attrs::RepRepe),
        );
        info[0x6E] = OpcodeInfo(
            Ty::Op(Opcode::Outs, Signature::Dx_Yb, ModrmMask::empty()),
            enum_set!(Attrs::RepRepe),
        );
        info[0x6F] = OpcodeInfo(
            Ty::Op(Opcode::Outs, Signature::Dx_Yv, ModrmMask::empty()),
            enum_set!(Attrs::RepRepe),
        );
    }

    // FIXME: Correct attrs for Jcc
    // 0x70
    info[0x70] = OpcodeInfo(
        Ty::Op(Opcode::Jcc, Signature::Jb_o, ModrmMask::empty()),
        enum_set!(Attrs::TestOf),
    );
    info[0x71] = OpcodeInfo(
        Ty::Op(Opcode::Jcc, Signature::Jb_no, ModrmMask::empty()),
        enum_set!(Attrs::TestOf),
    );
    info[0x72] = OpcodeInfo(
        Ty::Op(Opcode::Jcc, Signature::Jb_b, ModrmMask::empty()),
        enum_set!(Attrs::TestOf),
    );
    info[0x73] = OpcodeInfo(
        Ty::Op(Opcode::Jcc, Signature::Jb_nb, ModrmMask::empty()),
        enum_set!(Attrs::TestCf),
    );
    info[0x74] = OpcodeInfo(
        Ty::Op(Opcode::Jcc, Signature::Jb_z, ModrmMask::empty()),
        enum_set!(Attrs::TestZf),
    );
    info[0x75] = OpcodeInfo(
        Ty::Op(Opcode::Jcc, Signature::Jb_nz, ModrmMask::empty()),
        enum_set!(Attrs::TestZf),
    );
    info[0x76] = OpcodeInfo(
        Ty::Op(Opcode::Jcc, Signature::Jb_e, ModrmMask::empty()),
        enum_set!(Attrs::TestCf),
    );
    info[0x77] = OpcodeInfo(
        Ty::Op(Opcode::Jcc, Signature::Jb_ne, ModrmMask::empty()),
        enum_set!(Attrs::TestOf),
    );

    // 0x78
    info[0x78] = OpcodeInfo(
        Ty::Op(Opcode::Jcc, Signature::Jb_s, ModrmMask::empty()),
        enum_set!(Attrs::TestOf),
    );
    info[0x79] = OpcodeInfo(
        Ty::Op(Opcode::Jcc, Signature::Jb_ns, ModrmMask::empty()),
        enum_set!(Attrs::TestOf),
    );
    info[0x7A] = OpcodeInfo(
        Ty::Op(Opcode::Jcc, Signature::Jb_p, ModrmMask::empty()),
        enum_set!(Attrs::TestOf),
    );
    info[0x7B] = OpcodeInfo(
        Ty::Op(Opcode::Jcc, Signature::Jb_np, ModrmMask::empty()),
        enum_set!(Attrs::TestOf),
    );
    info[0x7C] = OpcodeInfo(
        Ty::Op(Opcode::Jcc, Signature::Jb_l, ModrmMask::empty()),
        enum_set!(Attrs::TestOf),
    );
    info[0x7D] = OpcodeInfo(
        Ty::Op(Opcode::Jcc, Signature::Jb_nl, ModrmMask::empty()),
        enum_set!(Attrs::TestOf),
    );
    info[0x7E] = OpcodeInfo(
        Ty::Op(Opcode::Jcc, Signature::Jb_le, ModrmMask::empty()),
        enum_set!(Attrs::TestOf),
    );
    info[0x7F] = OpcodeInfo(
        Ty::Op(Opcode::Jcc, Signature::Jb_nle, ModrmMask::empty()),
        enum_set!(Attrs::TestOf),
    );

    // FIXME: Make sure revised opcodes specify modrm use.
    // 0x80
    info[0x80] = OpcodeInfo(
        Ty::Revise(Group::Group1Eb_Ib, ModrmMask::empty()),
        enum_set!(Attrs::UseModrm | Attrs::Lock),
    );
    info[0x81] = OpcodeInfo(
        Ty::Revise(Group::Group1Ev_Iv, ModrmMask::empty()),
        enum_set!(Attrs::UseModrm | Attrs::Lock),
    );
    // TODO: 0x82: empty (I think this is undefined?)
    info[0x83] = OpcodeInfo(
        Ty::Revise(Group::Group1Ev_IbExtend, ModrmMask::empty()),
        enum_set!(Attrs::UseModrm | Attrs::Lock),
    );
    info[0x84] = OpcodeInfo(
        Ty::Op(Opcode::Test, Signature::Eb_Gb, ModrmMask::empty()),
        Attrs::TEST_MODRM,
    );
    info[0x85] = OpcodeInfo(
        Ty::Op(Opcode::Test, Signature::Ev_Gv, ModrmMask::empty()),
        Attrs::TEST_MODRM,
    );
    info[0x86] = OpcodeInfo(
        Ty::Op(Opcode::Xchg, Signature::Eb_Gb, ModrmMask::empty()),
        enum_set!(Attrs::UseModrm),
    );
    info[0x87] = OpcodeInfo(
        Ty::Op(Opcode::Xchg, Signature::Ev_Gv, ModrmMask::empty()),
        enum_set!(Attrs::UseModrm),
    );

    // 0x88
    info[0x88] = OpcodeInfo(
        Ty::Op(Opcode::Mov, Signature::Eb_Gb, ModrmMask::empty()),
        enum_set!(Attrs::UseModrm),
    );
    info[0x89] = OpcodeInfo(
        Ty::Op(Opcode::Mov, Signature::Ev_Gv, ModrmMask::empty()),
        enum_set!(Attrs::UseModrm),
    );
    info[0x8A] = OpcodeInfo(
        Ty::Op(Opcode::Mov, Signature::Gb_Eb, ModrmMask::empty()),
        enum_set!(Attrs::UseModrm),
    );
    info[0x8B] = OpcodeInfo(
        Ty::Op(Opcode::Mov, Signature::Gv_Ev, ModrmMask::empty()),
        enum_set!(Attrs::UseModrm),
    );
    info[0x8C] = OpcodeInfo(
        Ty::Op(Opcode::Mov, Signature::Ew_Sw, ModrmMask::empty()),
        enum_set!(Attrs::UseModrm),
    );
    info[0x8D] = OpcodeInfo(
        Ty::Op(Opcode::Lea, Signature::Gv_M, ModrmMask::empty()),
        enum_set!(Attrs::UseModrm),
    );
    info[0x8E] = OpcodeInfo(
        Ty::Op(Opcode::Mov, Signature::Sw_Ew, ModrmMask::empty()),
        enum_set!(Attrs::UseModrm),
    );
    info[0x8F] = OpcodeInfo(
        Ty::Op(Opcode::Pop, Signature::Ew, ModrmMask::empty()),
        enum_set!(Attrs::UseModrm),
    );

    // 0x90
    info[0x90] = OpcodeInfo(Ty::Op(Opcode::Nop, Signature::Empty, ModrmMask::empty()), enum_set!());
    info[0x91] = OpcodeInfo(Ty::Op(Opcode::Xchg, Signature::eCx, ModrmMask::empty()), enum_set!());
    info[0x92] = OpcodeInfo(Ty::Op(Opcode::Xchg, Signature::eDx, ModrmMask::empty()), enum_set!());
    info[0x93] = OpcodeInfo(Ty::Op(Opcode::Xchg, Signature::eBx, ModrmMask::empty()), enum_set!());
    info[0x94] = OpcodeInfo(Ty::Op(Opcode::Xchg, Signature::eSp, ModrmMask::empty()), enum_set!());
    info[0x95] = OpcodeInfo(Ty::Op(Opcode::Xchg, Signature::eBp, ModrmMask::empty()), enum_set!());
    info[0x96] = OpcodeInfo(Ty::Op(Opcode::Xchg, Signature::eSi, ModrmMask::empty()), enum_set!());
    info[0x97] = OpcodeInfo(Ty::Op(Opcode::Xchg, Signature::eDi, ModrmMask::empty()), enum_set!());

    // 0x98
    info[0x98] = OpcodeInfo(
        Ty::Op(Opcode::CbwCbwe, Signature::Empty, ModrmMask::empty()),
        enum_set!(),
    );
    info[0x99] = OpcodeInfo(
        Ty::Op(Opcode::CwdCdq, Signature::Empty, ModrmMask::empty()),
        enum_set!(),
    );
    info[0x9A] = OpcodeInfo(Ty::Op(Opcode::Call, Signature::Ap, ModrmMask::empty()), enum_set!());
    info[0x9B] = OpcodeInfo(Ty::Op(Opcode::Wait, Signature::Empty, ModrmMask::empty()), enum_set!());
    info[0x9C] = OpcodeInfo(
        Ty::Op(Opcode::Pushf, Signature::Empty, ModrmMask::empty()),
        Attrs::PUSHF,
    );
    info[0x9D] = OpcodeInfo(Ty::Op(Opcode::Popf, Signature::Empty, ModrmMask::empty()), Attrs::POPF);
    info[0x9E] = OpcodeInfo(Ty::Op(Opcode::Sahf, Signature::Empty, ModrmMask::empty()), Attrs::POPF);
    info[0x9F] = OpcodeInfo(Ty::Op(Opcode::Lahf, Signature::Empty, ModrmMask::empty()), Attrs::PUSHF);

    // 0xA0
    info[0xA0] = OpcodeInfo(Ty::Op(Opcode::Mov, Signature::Al_Ob, ModrmMask::empty()), enum_set!());
    info[0xA1] = OpcodeInfo(Ty::Op(Opcode::Mov, Signature::eAx_Ov, ModrmMask::empty()), enum_set!());
    info[0xA2] = OpcodeInfo(Ty::Op(Opcode::Mov, Signature::Ob_Al, ModrmMask::empty()), enum_set!());
    info[0xA3] = OpcodeInfo(Ty::Op(Opcode::Mov, Signature::Ov_eAx, ModrmMask::empty()), enum_set!());
    info[0xA4] = OpcodeInfo(
        Ty::Op(Opcode::Movs, Signature::Xb_Yb, ModrmMask::empty()),
        enum_set!(Attrs::RepRepe),
    );
    info[0xA5] = OpcodeInfo(
        Ty::Op(Opcode::Movs, Signature::Xv_Yv, ModrmMask::empty()),
        enum_set!(Attrs::RepRepe),
    );
    info[0xA6] = OpcodeInfo(
        Ty::Op(Opcode::Cmps, Signature::Xb_Yb, ModrmMask::empty()),
        Attrs::REPNE_REPE,
    );
    info[0xA7] = OpcodeInfo(
        Ty::Op(Opcode::Cmps, Signature::Xv_Yv, ModrmMask::empty()),
        Attrs::REPNE_REPE,
    );

    // 0xA8
    info[0xA8] = OpcodeInfo(Ty::Op(Opcode::Test, Signature::Al_Ib, ModrmMask::empty()), enum_set!());
    info[0xA9] = OpcodeInfo(Ty::Op(Opcode::Test, Signature::eAx_Iv, ModrmMask::empty()), enum_set!());
    info[0xAA] = OpcodeInfo(
        Ty::Op(Opcode::Stos, Signature::Yb_Al, ModrmMask::empty()),
        enum_set!(Attrs::RepRepe),
    );
    info[0xAB] = OpcodeInfo(
        Ty::Op(Opcode::Stos, Signature::Yv_eAx, ModrmMask::empty()),
        enum_set!(Attrs::RepRepe),
    );
    info[0xAC] = OpcodeInfo(Ty::Op(Opcode::Lods, Signature::Al_Xb, ModrmMask::empty()), enum_set!());
    info[0xAD] = OpcodeInfo(Ty::Op(Opcode::Lods, Signature::eAx_Xv, ModrmMask::empty()), enum_set!());
    info[0xAE] = OpcodeInfo(
        Ty::Op(Opcode::Scas, Signature::Al_Xb, ModrmMask::empty()),
        Attrs::REPNE_REPE,
    );
    info[0xAF] = OpcodeInfo(
        Ty::Op(Opcode::Scas, Signature::eAx_Xv, ModrmMask::empty()),
        Attrs::REPNE_REPE,
    );

    // 0xB0
    info[0xB0] = OpcodeInfo(Ty::Op(Opcode::Mov, Signature::Al, ModrmMask::empty()), enum_set!());
    info[0xB1] = OpcodeInfo(Ty::Op(Opcode::Mov, Signature::Cl, ModrmMask::empty()), enum_set!());
    info[0xB2] = OpcodeInfo(Ty::Op(Opcode::Mov, Signature::Dl, ModrmMask::empty()), enum_set!());
    info[0xB3] = OpcodeInfo(Ty::Op(Opcode::Mov, Signature::Bl, ModrmMask::empty()), enum_set!());
    info[0xB4] = OpcodeInfo(Ty::Op(Opcode::Mov, Signature::Ah, ModrmMask::empty()), enum_set!());
    info[0xB5] = OpcodeInfo(Ty::Op(Opcode::Mov, Signature::Ch, ModrmMask::empty()), enum_set!());
    info[0xB6] = OpcodeInfo(Ty::Op(Opcode::Mov, Signature::Dh, ModrmMask::empty()), enum_set!());
    info[0xB7] = OpcodeInfo(Ty::Op(Opcode::Mov, Signature::Bh, ModrmMask::empty()), enum_set!());

    // 0xB8
    info[0xB8] = OpcodeInfo(Ty::Op(Opcode::Mov, Signature::eAx, ModrmMask::empty()), enum_set!());
    info[0xB9] = OpcodeInfo(Ty::Op(Opcode::Mov, Signature::eCx, ModrmMask::empty()), enum_set!());
    info[0xBA] = OpcodeInfo(Ty::Op(Opcode::Mov, Signature::eDx, ModrmMask::empty()), enum_set!());
    info[0xBB] = OpcodeInfo(Ty::Op(Opcode::Mov, Signature::eBx, ModrmMask::empty()), enum_set!());
    info[0xBC] = OpcodeInfo(Ty::Op(Opcode::Mov, Signature::eSp, ModrmMask::empty()), enum_set!());
    info[0xBD] = OpcodeInfo(Ty::Op(Opcode::Mov, Signature::eBp, ModrmMask::empty()), enum_set!());
    info[0xBE] = OpcodeInfo(Ty::Op(Opcode::Mov, Signature::eSi, ModrmMask::empty()), enum_set!());
    info[0xBF] = OpcodeInfo(Ty::Op(Opcode::Mov, Signature::eDi, ModrmMask::empty()), enum_set!());

    // 0xC0
    info[0xC0] = OpcodeInfo(
        Ty::Revise(Group::Group2Gb_Eb, ModrmMask::FORBID_EXT_110),
        enum_set!(Attrs::UseModrm),
    );
    info[0xC1] = OpcodeInfo(
        Ty::Revise(Group::Group2Gv_Ev, ModrmMask::FORBID_EXT_110),
        enum_set!(Attrs::UseModrm),
    );
    info[0xC2] = OpcodeInfo(Ty::Op(Opcode::RetNear, Signature::Iv, ModrmMask::empty()), enum_set!());
    info[0xC3] = OpcodeInfo(
        Ty::Op(Opcode::RetNear, Signature::Empty, ModrmMask::empty()),
        enum_set!(),
    );
    // TODO (C4)
    // TODO (C5)
    info[0xC6] = OpcodeInfo(
        Ty::Op(Opcode::Mov, Signature::Eb_Ib, ModrmMask::empty()),
        enum_set!(Attrs::UseModrm),
    );
    info[0xC7] = OpcodeInfo(
        Ty::Op(Opcode::Mov, Signature::Ev_Iv, ModrmMask::empty()),
        enum_set!(Attrs::UseModrm),
    );

    // 0xC8
    if options.isa.is_186() {
        info[0xC8] = OpcodeInfo(Ty::Op(Opcode::Enter, Signature::Iw_Ib, ModrmMask::empty()), enum_set!());
        info[0xC9] = OpcodeInfo(Ty::Op(Opcode::Leave, Signature::Empty, ModrmMask::empty()), enum_set!());
    }
    info[0xCA] = OpcodeInfo(Ty::Op(Opcode::RetFar, Signature::Iv, ModrmMask::empty()), enum_set!());
    info[0xCB] = OpcodeInfo(
        Ty::Op(Opcode::RetFar, Signature::Empty, ModrmMask::empty()),
        enum_set!(),
    );
    info[0xCC] = OpcodeInfo(Ty::Op(Opcode::Int3, Signature::Empty, ModrmMask::empty()), enum_set!());
    info[0xCD] = OpcodeInfo(Ty::Op(Opcode::Int, Signature::Ib, ModrmMask::empty()), enum_set!());
    info[0xCE] = OpcodeInfo(Ty::Op(Opcode::Into, Signature::Empty, ModrmMask::empty()), enum_set!());
    info[0xCF] = OpcodeInfo(Ty::Op(Opcode::Iret, Signature::Empty, ModrmMask::empty()), enum_set!());

    // 0xD0
    info[0xD0] = OpcodeInfo(
        Ty::Revise(Group::Group2Eb_1, ModrmMask::FORBID_EXT_110),
        enum_set!(Attrs::UseModrm),
    );
    info[0xD1] = OpcodeInfo(
        Ty::Revise(Group::Group2Ev_1, ModrmMask::FORBID_EXT_110),
        enum_set!(Attrs::UseModrm),
    );
    info[0xD2] = OpcodeInfo(
        Ty::Revise(Group::Group2Eb_Cl, ModrmMask::FORBID_EXT_110),
        enum_set!(Attrs::UseModrm),
    );
    info[0xD3] = OpcodeInfo(
        Ty::Revise(Group::Group2Ev_Cl, ModrmMask::FORBID_EXT_110),
        enum_set!(Attrs::UseModrm),
    );
    info[0xD4] = match options.isa.is_v30() {
        true => OpcodeInfo(
            Ty::Op(Opcode::AamNec, Signature::Empty, ModrmMask::empty()),
            Attrs::AAM_AAD,
        ),
        false => OpcodeInfo(Ty::Op(Opcode::Aam, Signature::Ib, ModrmMask::empty()), Attrs::AAM_AAD),
    };
    info[0xD5] = match options.isa.is_v30() {
        true => OpcodeInfo(
            Ty::Op(Opcode::AadNec, Signature::Empty, ModrmMask::empty()),
            Attrs::AAM_AAD,
        ),
        false => OpcodeInfo(Ty::Op(Opcode::Aad, Signature::Ib, ModrmMask::empty()), Attrs::AAM_AAD),
    };
    // 0xD6: empty
    info[0xD7] = OpcodeInfo(Ty::Op(Opcode::Xlat, Signature::Empty, ModrmMask::empty()), enum_set!());

    // 0xD8
    // TODO: Fpu

    // 0xE0
    // FIXME: This operand signature is wrong...
    info[0xE0] = OpcodeInfo(
        Ty::Op(Opcode::Loopne, Signature::Jb_z, ModrmMask::empty()),
        Attrs::LOOPE,
    );
    info[0xE1] = OpcodeInfo(Ty::Op(Opcode::Loope, Signature::Jb_z, ModrmMask::empty()), Attrs::LOOPE);
    info[0xE2] = OpcodeInfo(Ty::Op(Opcode::Loop, Signature::Jb_z, ModrmMask::empty()), enum_set!());
    info[0xE3] = OpcodeInfo(
        Ty::Op(Opcode::JcxzJecxz, Signature::Jb_z, ModrmMask::empty()),
        Attrs::JCXZ,
    );
    info[0xE4] = OpcodeInfo(Ty::Op(Opcode::In, Signature::Al_Ib, ModrmMask::empty()), enum_set!());
    info[0xE5] = OpcodeInfo(Ty::Op(Opcode::In, Signature::eAx_Iv, ModrmMask::empty()), enum_set!());
    info[0xE6] = OpcodeInfo(Ty::Op(Opcode::Out, Signature::Ib_Al, ModrmMask::empty()), enum_set!());
    info[0xE7] = OpcodeInfo(Ty::Op(Opcode::Out, Signature::Iv_eAx, ModrmMask::empty()), enum_set!());

    // 0xE8
    info[0xE8] = OpcodeInfo(Ty::Op(Opcode::Call, Signature::Av, ModrmMask::empty()), enum_set!());
    // TODO (E9)
    // TODO (EA)
    info[0xEA] = OpcodeInfo(Ty::Op(Opcode::Jmp, Signature::Ap, ModrmMask::empty()), enum_set!());
    // TODO (EB)
    info[0xEC] = OpcodeInfo(Ty::Op(Opcode::In, Signature::Al_Dx, ModrmMask::empty()), enum_set!());
    info[0xED] = OpcodeInfo(Ty::Op(Opcode::In, Signature::eAx_Dx, ModrmMask::empty()), enum_set!());
    info[0xEE] = OpcodeInfo(Ty::Op(Opcode::Out, Signature::Dx_Al, ModrmMask::empty()), enum_set!());
    info[0xEF] = OpcodeInfo(Ty::Op(Opcode::Out, Signature::Dx_eAx, ModrmMask::empty()), enum_set!());

    // 0xF0
    info[0xF0] = OpcodeInfo(Ty::Prefix, enum_set!());
    // 0xF1: Empty
    info[0xF2] = OpcodeInfo(Ty::Prefix, enum_set!());
    info[0xF3] = OpcodeInfo(Ty::Prefix, enum_set!());
    info[0xF4] = OpcodeInfo(Ty::Op(Opcode::Hlt, Signature::Empty, ModrmMask::empty()), enum_set!());
    info[0xF5] = OpcodeInfo(Ty::Op(Opcode::Cmc, Signature::Empty, ModrmMask::empty()), enum_set!());
    // TODO (F6): Group3
    // TODO (F7): Group3

    // 0xF8
    info[0xF8] = OpcodeInfo(
        Ty::Op(Opcode::Clc, Signature::Empty, ModrmMask::empty()),
        enum_set!(Attrs::ZeroCf),
    );
    info[0xF9] = OpcodeInfo(
        Ty::Op(Opcode::Stc, Signature::Empty, ModrmMask::empty()),
        enum_set!(Attrs::ModifyCf),
    );
    info[0xFA] = OpcodeInfo(Ty::Op(Opcode::Cli, Signature::Empty, ModrmMask::empty()), enum_set!());
    info[0xFB] = OpcodeInfo(Ty::Op(Opcode::Sti, Signature::Empty, ModrmMask::empty()), enum_set!());
    info[0xFC] = OpcodeInfo(Ty::Op(Opcode::Cld, Signature::Empty, ModrmMask::empty()), enum_set!());
    info[0xFD] = OpcodeInfo(Ty::Op(Opcode::Std, Signature::Empty, ModrmMask::empty()), enum_set!());
    info[0xFE] = OpcodeInfo(
        Ty::Revise(Group::Group4Eb, ModrmMask::GROUP4),
        Attrs::INC_DEC_MODRM_LOCK,
    );
    // FIXME: Is LOCK technically valid here? Should revised opcodes even have a lock check?
    //
    // FIXME: Ev is wrong for far CALL and far JMP, as the signature is a full displacement
    info[0xFF] = OpcodeInfo(
        Ty::Revise(Group::Group5, ModrmMask::GROUP5),
        enum_set!(Attrs::UseModrm | Attrs::Lock),
    );
    info
}

#[allow(clippy::let_and_return)] // not done yet
pub const fn byte_2(_options: OpcodeOptions) -> [OpcodeInfo; 256] {
    let info = [OpcodeInfo(Ty::None, enum_set!()); 256];
    info
}
