//! x86 Instruction decoding and disassembly
//!
//! x86 instructions are relatively difficult to decode. This is partially a result of the x86's encoding
//! that includes variable size encodings, instruction prefixes and the extension to 32 and then again
//! 64-bits. Thankfully there were no 64-bit PC-98 machines. Late PC-98 machines only use a subset of the
//! modern 32-bit ISA as most PC-98 machines only went up to Pentium and Celeron processors. There is the
//! PC98-NX series of machines, but that linage is descended from the IBM PC meaning we do not need to
//! consider ISA changes introduced in those machines.
//!
//! First, a look at the Intel SDM for the instruction encoding:
//!
//! | Prefixes | Opcode | ModR/M | SIB | Displacement | Immediate |
//! |----------|--------|--------|-----|--------------|-----------|
//! || Either 1 byte or two bytes|if required, sometimes partially used as an opcode extension|if required|1, 2, 4 or no bytes|1, 2, 4 or no bytes|
//!
//! However this table does not explain the entire process of decoding an instruction.
//!
//! Starting with prefixes, these may influence later stages of the instruction decode pipeline. The
//! quintessential example of this is the address override prefix (`0x67`). When the address override prefix
//! is present, the interpretation of the ModR/M byte changes. If the address size becomes 32-bit (either by
//! entering protected mode or using the address override prefix in real mode) then an instruction like
//! `ADD BYTE [bx + si], al` in real mode (`0x00 0x00`) will change to `ADD BYTE [eax], al`.
//!
//! The operand size override prefix (`0x66`) may affect which opcode is interpreted. When addressing memory,
//! segment prefix overrides may also influence which address is read if segmentation is used (hint: it will
//! always will be used somehow pre-x64, either as an offset in real mode or specify the memory descriptor in
//! the global descriptor table).
//!
//! Once prefixes are handled, then the opcode comes next. In x86 the opcode may be 1 or 2 bytes. Some opcodes
//! are special (such as ALU operations with part of ModR/M byte and x87 FPU instructions) and use part of
//! the ModR/M byte as an extension to the opcode.
//!
//! Finally in some memory addressing modes, a SIB (scale-index-base) byte and displacement may be present.
//!
//! # The decoding pipeline
//!
//! A two stage decoding pipeline is used:
//! 1. Read prefixes and use a fixed function lookup tables to get opcodes, signatures and modrm masks
//! 2. Match the opcode to and invoke functions to decode the rest of the instruction
//!
//! In the first stage, prefixes and the opcode are read using the provided [`DecoderTables`]. The info read
//! from the tables can be used to eliminate certain types of invalid instructions quickly such as invalid opcodes
//! and prefixes (such as an instruction which can't be locked). The first stage also handles "group" instructions
//! such as `80h`, `81h` and `83h`. For group opcodes, the second stage revises the opcode to form a valid instruction.
//!
//! The second stage is relatively simple: match on the opcode and signature and read the rest of the instruction.

#![no_std]
#![deny(clippy::disallowed_methods, clippy::pub_use)]

mod opcode;

use core::mem;

use enumset::{enum_set, EnumSet, EnumSetType};
use x86_types::{
    CodeSize, Cond, Flags, Instruction, Isa, Memory, Modrm, Opcode, Operands, Reg16, Reg32, Reg8, RepTy, Segment,
};

/// Options used to generate decoder tables for opcodes.
#[derive(Debug, Clone, Copy)]
pub struct OpcodeOptions {
    /// The processor micro-architecture.
    pub isa: Isa,

    /// Should undefined instructions be added to the table?
    pub allow_undefined: bool,
}

/// Opcode description without any operands.
#[derive(Debug, Clone, Copy)]
pub struct OpcodeInfo(Ty, EnumSet<Attrs>);

impl OpcodeInfo {
    /// The opcode type.
    pub const fn ty(self) -> Ty {
        self.0
    }

    /// Opcode allows lock prefix with memory operand
    pub fn lockable(self) -> bool {
        self.1.contains(Attrs::Lock)
    }

    /// Opcode allows a `repne` prefix.
    pub fn repne(self) -> bool {
        self.1.contains(Attrs::Repne)
    }

    /// Opcode allows a `rep` or `repe` prefix.
    pub fn rep(self) -> bool {
        self.1.contains(Attrs::RepRepe)
    }

    /// Flags which are clobbered.
    pub fn clobbers(self) -> Flags {
        let mut clobbers = Flags::empty();
        let attrs = self.1;

        // from_bits_retain will be needed until impl const
        if attrs.contains(Attrs::ClobberCf) {
            clobbers = Flags::from_bits_retain(clobbers.bits() | Flags::CF.bits());
        }

        if attrs.contains(Attrs::ClobberPf) {
            clobbers = Flags::from_bits_retain(clobbers.bits() | Flags::PF.bits());
        }

        if attrs.contains(Attrs::ClobberAf) {
            clobbers = Flags::from_bits_retain(clobbers.bits() | Flags::AF.bits());
        }

        if attrs.contains(Attrs::ClobberZf) {
            clobbers = Flags::from_bits_retain(clobbers.bits() | Flags::ZF.bits());
        }

        if attrs.contains(Attrs::ClobberSf) {
            clobbers = Flags::from_bits_retain(clobbers.bits() | Flags::SF.bits());
        }

        if attrs.contains(Attrs::ClobberOf) {
            clobbers = Flags::from_bits_retain(clobbers.bits() | Flags::OF.bits());
        }

        clobbers
    }

    /// Flags which are tested.
    pub fn tests(self) -> Flags {
        let mut tests = Flags::empty();
        let attrs = self.1;

        // from_bits_retain will be needed until impl const
        if attrs.contains(Attrs::TestCf) {
            tests = Flags::from_bits_retain(tests.bits() | Flags::CF.bits());
        }

        if attrs.contains(Attrs::TestPf) {
            tests = Flags::from_bits_retain(tests.bits() | Flags::PF.bits());
        }

        if attrs.contains(Attrs::TestAf) {
            tests = Flags::from_bits_retain(tests.bits() | Flags::AF.bits());
        }

        if attrs.contains(Attrs::TestZf) {
            tests = Flags::from_bits_retain(tests.bits() | Flags::ZF.bits());
        }

        if attrs.contains(Attrs::TestSf) {
            tests = Flags::from_bits_retain(tests.bits() | Flags::SF.bits());
        }

        if attrs.contains(Attrs::TestOf) {
            tests = Flags::from_bits_retain(tests.bits() | Flags::OF.bits());
        }

        tests
    }

    /// Flags which are modified.
    pub fn modifies(self) -> Flags {
        let mut modifies = Flags::empty();
        let attrs = self.1;

        // from_bits_retain will be needed until impl const
        if attrs.contains(Attrs::ModifyCf) {
            modifies = Flags::from_bits_retain(modifies.bits() | Flags::CF.bits());
        }

        if attrs.contains(Attrs::ModifyPf) {
            modifies = Flags::from_bits_retain(modifies.bits() | Flags::PF.bits());
        }

        if attrs.contains(Attrs::ModifyAf) {
            modifies = Flags::from_bits_retain(modifies.bits() | Flags::AF.bits());
        }

        if attrs.contains(Attrs::ModifyZf) {
            modifies = Flags::from_bits_retain(modifies.bits() | Flags::ZF.bits());
        }

        if attrs.contains(Attrs::ModifySf) {
            modifies = Flags::from_bits_retain(modifies.bits() | Flags::SF.bits());
        }

        if attrs.contains(Attrs::ModifyOf) {
            modifies = Flags::from_bits_retain(modifies.bits() | Flags::OF.bits());
        }

        modifies
    }

    /// Flags which are zeroed.
    pub fn zeroes(self) -> Flags {
        let mut zeroes = Flags::empty();
        let attrs = self.1;

        // from_bits_retain will be needed until impl const
        if attrs.contains(Attrs::ZeroCf) {
            zeroes = Flags::from_bits_retain(zeroes.bits() | Flags::CF.bits());
        }

        if attrs.contains(Attrs::ZeroPf) {
            zeroes = Flags::from_bits_retain(zeroes.bits() | Flags::PF.bits());
        }

        if attrs.contains(Attrs::ZeroAf) {
            zeroes = Flags::from_bits_retain(zeroes.bits() | Flags::AF.bits());
        }

        if attrs.contains(Attrs::ZeroZf) {
            zeroes = Flags::from_bits_retain(zeroes.bits() | Flags::ZF.bits());
        }

        if attrs.contains(Attrs::ZeroSf) {
            zeroes = Flags::from_bits_retain(zeroes.bits() | Flags::SF.bits());
        }

        if attrs.contains(Attrs::ZeroOf) {
            zeroes = Flags::from_bits_retain(zeroes.bits() | Flags::OF.bits());
        }

        zeroes
    }

    /// The instruction uses the modrm byte.
    pub fn uses_modrm(self) -> bool {
        self.1.contains(Attrs::UseModrm)
    }
}

/// Instruction groups.
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
#[allow(non_camel_case_types)]
pub enum Group {
    /// Group 1 [`Signature::Eb_Ib`]
    Group1Eb_Ib,

    /// Group 1 [`Signature::Ev_Iv`]
    Group1Ev_Iv,

    /// Group 1 [`Signature::Ev_IbExtend`]
    Group1Ev_IbExtend,

    /// Group 2 [`Signature::Gb_Eb`]
    Group2Gb_Eb,

    /// Group 2 [`Signature::Gv_Ev`]
    Group2Gv_Ev,

    /// Group 2 [`Signature::Eb_1`]
    Group2Eb_1,

    /// Group 2 [`Signature::Ev_1`]
    Group2Ev_1,

    /// Group 2 [`Signature::Eb_Cl`]
    Group2Eb_Cl,

    /// Group 2 [`Signature::Ev_Cl`]
    Group2Ev_Cl,

    /// Group 3 [`Signature::Eb`]
    Group3Eb,

    /// Group 3 [`Signature::Ev`]
    Group3Ev,

    /// Group 4 [`Signature::Eb`]
    Group4Eb,

    /// Group 5 [`Signature::Ev`] and [`Signature::Ep`]
    Group5,
}

bitflags::bitflags! {
    /// Mask of forbidden modr/m values
    #[derive(Debug, Clone, Copy)]
    pub struct ModrmMask: u8 {
        const FORBID_EXT_000 = 1 << 0;
        const FORBID_EXT_001 = 1 << 1;
        const FORBID_EXT_010 = 1 << 2;
        const FORBID_EXT_011 = 1 << 3;
        const FORBID_EXT_100 = 1 << 4;
        const FORBID_EXT_101 = 1 << 5;
        const FORBID_EXT_110 = 1 << 6;
        const FORBID_EXT_111 = 1 << 7;

        const GROUP3 = ModrmMask::FORBID_EXT_001.bits();
        const GROUP4 = ModrmMask::FORBID_EXT_010.bits()
            | ModrmMask::FORBID_EXT_011.bits()
            | ModrmMask::FORBID_EXT_100.bits()
            | ModrmMask::FORBID_EXT_101.bits()
            | ModrmMask::FORBID_EXT_110.bits()
            | ModrmMask::FORBID_EXT_111.bits();
        const GROUP5 = ModrmMask::FORBID_EXT_111.bits();
    }
}

#[derive(Debug, Clone, Copy)]
#[repr(u8)]
pub enum Ty {
    /// There is no opcode.
    None = 0, // explicitly set a tag of 0 so LLVM doesn't need to do a bunch of writes.

    /// The instruction is an opcode.
    Op(Opcode, Signature, ModrmMask),

    /// The instruction is likely a valid opcode, but needs to be revised using information found further in
    /// the instruction.
    ///
    /// The opcode's [`Signature`] is also determined after revising the opcode.
    Revise(Group, ModrmMask),

    /// The opcode is a prefix.
    Prefix,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum DecodeError {
    // TODO: Spans?
    InvalidOpcode,

    InvalidPrefix,

    OutOfBytes,

    TooLong,
}

/// An operand signature.
#[allow(non_camel_case_types)]
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum Signature {
    /// No operands.
    Empty,

    Eb,

    Ev,

    /// Full displacement
    Ep,

    /// `r/m8 r8`
    Eb_Gb,

    /// `r/m16 r16` or `r/m32 r32`
    Ev_Gv,

    /// `r/m16 r16`. This is used in ARPL.
    Ew_Rw,

    /// `r8 r/m8`
    Gb_Eb,

    /// `r16 r/m16` or `r32 r/m32`
    Gv_Ev,

    /// `AL imm8`
    Al_Ib,

    /// `AX imm16` or `EAX imm32`
    eAx_Iv,

    Ib_Al,

    Iv_eAx,

    /// `ES`
    Es,

    /// `CS`
    Cs,

    /// `SS`
    Ss,

    /// `DS`
    Ds,

    /// `FS`
    Fs,

    /// `GS`
    Gs,

    Al,

    Cl,

    Dl,

    Bl,

    Ah,

    Ch,

    Dh,

    Bh,

    eAx,

    eCx,

    eDx,

    eBx,

    eSp,

    eBp,

    eSi,

    eDi,

    Ib,

    Iv,

    Gv_Ma,

    Gv_Ev_Iv,

    Gv_Ev_IbExtend,

    Yb_Dx,

    Yv_Dx,

    Dx_Yb,

    Dx_Yv,

    Jb_o,

    Jb_no,

    Jb_b,

    Jb_nb,

    Jb_z,

    Jb_nz,

    Jb_e,

    Jb_ne,

    Jb_s,

    Jb_ns,

    Jb_p,

    Jb_np,

    Jb_l,

    Jb_nl,

    Jb_le,

    Jb_nle,

    Eb_Ib,

    Ev_Iv,

    /// Sign extended
    Ev_IbExtend,

    Ew_Sw,

    Sw_Ew,

    /// Only allows ext=000
    Ew,

    Gv_M,

    Ap,

    Al_Ob,

    eAx_Ov,

    Ob_Al,

    Ov_eAx,

    Xb_Yb,

    Xv_Yv,

    Yb_Al,

    Yv_eAx,

    Al_Xb,

    eAx_Xv,

    Iw_Ib,

    Eb_1,

    Ev_1,

    Eb_Cl,

    Ev_Cl,

    Av,

    Al_Dx,

    eAx_Dx,

    Dx_Al,

    Dx_eAx,
}

/// Opcode table generation.
pub enum OpcodeTables {}

impl OpcodeTables {
    /// Generate a table for byte one opcode mappings.
    pub const fn byte_1_table(options: OpcodeOptions) -> [OpcodeInfo; 256] {
        opcode::byte_1(options)
    }

    /// Generate a table for byte two opcode mappings.
    pub const fn byte_2_table(options: OpcodeOptions) -> [OpcodeInfo; 256] {
        assert!(
            options.isa.is_286(),
            "If the ISA is not 286 or greater, the 2nd byte table is invalid"
        );

        opcode::byte_2(options)
    }
}

#[derive(Debug, EnumSetType)]
#[enumset(repr = "u32")]
enum Attrs {
    /// The instruction uses the modr/m byte.
    UseModrm,

    /// The lock prefix is valid.
    Lock,

    /// The REPNE prefix is valid.
    Repne,

    /// The REP or REPE prefix is valid.
    RepRepe,

    /// The instruction is undefined.
    Undefined,

    /// The `CF` flag is clobbered by the opcode.
    ClobberCf,

    /// The `PF` flag is clobbered by the opcode.
    ClobberPf,

    /// The `AF` flag is clobbered by the opcode.
    ClobberAf,

    /// The `ZF` flag is clobbered by the opcode.
    ClobberZf,

    /// The `SF` flag is clobbered by the opcode.
    ClobberSf,

    /// The `OF` flag is clobbered by the opcode.
    ClobberOf,

    /// The `CF` flag is tested by the opcode.
    TestCf,

    /// The `PF` flag is tested by the opcode.
    TestPf,

    /// The `AF` flag is tested by the opcode.
    TestAf,

    /// The `ZF` flag is tested by the opcode.
    TestZf,

    /// The `SF` flag is tested by the opcode.
    TestSf,

    /// The `OF` flag is tested by the opcode.
    TestOf,

    /// The `CF` flag is set or reset by the opcode.
    ModifyCf,

    /// The `PF` flag is set or reset by the opcode.
    ModifyPf,

    /// The `AF` flag is set or reset by the opcode.
    ModifyAf,

    /// The `ZF` flag is set or reset by the opcode.
    ModifyZf,

    /// The `SF` flag is set or reset by the opcode.
    ModifySf,

    /// The `OF` flag is set or reset by the opcode.
    ModifyOf,

    /// The `CF` flag is zeroed by the opcode.
    ZeroCf,

    /// The `PF` flag is zeroed by the opcode.
    ZeroPf,

    /// The `AF` flag is zeroed by the opcode.
    ZeroAf,

    /// The `ZF` flag is zeroed by the opcode.
    ZeroZf,

    /// The `SF` flag is zeroed by the opcode.
    ZeroSf,

    /// The `OF` flag is zeroed by the opcode.
    ZeroOf,
}

impl Attrs {
    const ALU: EnumSet<Self> =
        enum_set!(Self::ModifyCf | Self::ModifyPf | Self::ModifyAf | Self::ModifyZf | Self::ModifySf | Self::ModifyOf);

    const ALU_MODRM_LOCK: EnumSet<Self> = enum_set!(
        Self::UseModrm
            | Self::Lock
            | Self::ModifyCf
            | Self::ModifyPf
            | Self::ModifyAf
            | Self::ModifyZf
            | Self::ModifySf
            | Self::ModifyOf
    );

    const ALU_MODRM: EnumSet<Self> = enum_set!(
        Self::UseModrm
            | Self::ModifyCf
            | Self::ModifyPf
            | Self::ModifyAf
            | Self::ModifyZf
            | Self::ModifySf
            | Self::ModifyOf
    );

    const ALU_BORROW: EnumSet<Self> = enum_set!(
        Self::ModifyCf
            | Self::ModifyPf
            | Self::ModifyAf
            | Self::ModifyZf
            | Self::ModifySf
            | Self::ModifyOf
            | Self::TestCf
    );

    const ALU_BORROW_MODRM: EnumSet<Self> = enum_set!(
        Self::UseModrm
            | Self::ModifyCf
            | Self::ModifyPf
            | Self::ModifyAf
            | Self::ModifyZf
            | Self::ModifySf
            | Self::ModifyOf
            | Self::TestCf
    );

    const ALU_BORROW_MODRM_LOCK: EnumSet<Self> = enum_set!(
        Self::UseModrm
            | Self::Lock
            | Self::ModifyCf
            | Self::ModifyPf
            | Self::ModifyAf
            | Self::ModifyZf
            | Self::ModifySf
            | Self::ModifyOf
            | Self::TestCf
    );

    const BIT: EnumSet<Self> =
        enum_set!(Self::ZeroCf | Self::ModifyPf | Self::ClobberAf | Self::ModifySf | Self::ZeroOf);

    const BIT_MODRM: EnumSet<Self> =
        enum_set!(Self::UseModrm | Self::ZeroCf | Self::ModifyPf | Self::ClobberAf | Self::ModifySf | Self::ZeroOf);

    const BIT_MODRM_LOCK: EnumSet<Self> = enum_set!(
        Self::UseModrm | Self::Lock | Self::ZeroCf | Self::ModifyPf | Self::ClobberAf | Self::ModifySf | Self::ZeroOf
    );

    const BCD_ADJUST: EnumSet<Self> = enum_set!(
        Self::ClobberOf
            | Self::ModifySf
            | Self::ModifyZf
            | Self::TestAf
            | Self::ModifyAf
            | Self::ModifyPf
            | Self::TestCf
            | Self::ModifyPf
    );

    const ASCII_ADJUST: EnumSet<Self> = enum_set!(
        Self::ClobberOf
            | Self::ClobberSf
            | Self::ClobberZf
            | Self::TestAf
            | Self::ModifyAf
            | Self::ClobberPf
            | Self::ModifyCf
    );

    const INC_DEC: EnumSet<Self> =
        enum_set!(Self::ModifyOf | Self::ModifySf | Self::ModifyZf | Self::ModifyAf | Self::ModifyPf);

    const INC_DEC_MODRM_LOCK: EnumSet<Self> = enum_set!(
        Self::UseModrm
            | Self::Lock
            | Self::ModifyOf
            | Self::ModifySf
            | Self::ModifyZf
            | Self::ModifyAf
            | Self::ModifyPf
    );

    const IMUL: EnumSet<Self> = enum_set!(
        Self::UseModrm
            | Self::ModifyOf
            | Self::ClobberSf
            | Self::ClobberZf
            | Self::ClobberAf
            | Self::ClobberPf
            | Self::ModifyCf
    );

    const TEST: EnumSet<Self> =
        enum_set!(Self::ZeroOf | Self::ModifySf | Self::ModifyZf | Self::ClobberAf | Self::ModifyPf | Self::ZeroCf);

    const TEST_MODRM: EnumSet<Self> = enum_set!(
        Self::UseModrm
            | Self::ZeroOf
            | Self::ModifySf
            | Self::ModifyZf
            | Self::ClobberAf
            | Self::ModifyPf
            | Self::ZeroCf
    );

    const PUSHF: EnumSet<Self> =
        enum_set!(Self::TestOf | Self::TestSf | Self::TestZf | Self::TestAf | Self::TestPf | Self::TestCf);

    const POPF: EnumSet<Self> =
        enum_set!(Self::ModifyOf | Self::ModifySf | Self::ModifyZf | Self::ModifyAf | Self::ModifyPf | Self::ModifyCf);

    const AAM_AAD: EnumSet<Self> = enum_set!(Self::ModifySf | Self::ModifyZf | Self::ModifyPf);

    const LOOPE: EnumSet<Self> = enum_set!(Self::TestZf | Self::ModifyZf);

    const JCXZ: EnumSet<Self> = enum_set!(Self::TestZf);

    const REPNE_REPE: EnumSet<Self> = enum_set!(Self::Repne | Self::RepRepe);
}

#[derive(Debug)]
pub struct InstructionRaw {
    /// Description of the opcode.
    pub info: OpcodeInfo,

    /// Address size after decoding.
    pub address_size: CodeSize,

    /// Operand size after decoding.
    pub operand_size: CodeSize,

    /// Is a lock prefix present?
    pub lock: bool,

    /// Is a segment override prefix present?
    pub segment_override: Option<Segment>,

    pub rep: Option<RepTy>,
}

/// The bytes of a fetched instruction.
///
/// This just results in a simpler API.
pub struct FetchBuffer {
    bytes: [u8; 15],
    cursor: u8,
}

impl FetchBuffer {
    pub fn new(bytes: [u8; 15]) -> Self {
        Self { bytes, cursor: 0 }
    }

    pub fn with_slice(slice: &[u8]) -> Self {
        let mut bytes = [0; 15];
        bytes[0..slice.len()].copy_from_slice(slice);

        Self { bytes, cursor: 0 }
    }

    pub fn read_byte(&mut self, _anno: Annotation) -> Result<u8, DecodeError> {
        let byte = self.bytes.get(self.cursor as usize).ok_or(DecodeError::OutOfBytes)?;
        self.cursor += 1;
        Ok(*byte)
    }

    pub fn read_word(&mut self, _anno: Annotation) -> Result<u16, DecodeError> {
        let range = (self.cursor as usize)..(self.cursor as usize + mem::size_of::<u16>());
        let bytes =
            <[u8; 2]>::try_from(self.bytes.get(range).ok_or(DecodeError::OutOfBytes)?).expect("slice is 2 bytes long");
        let word = u16::from_le_bytes(bytes);
        self.cursor += 2;

        Ok(word)
    }

    pub fn read_dword(&mut self, _anno: Annotation) -> Result<u32, DecodeError> {
        let range = (self.cursor as usize)..(self.cursor as usize + mem::size_of::<u32>());
        let bytes =
            <[u8; 4]>::try_from(self.bytes.get(range).ok_or(DecodeError::OutOfBytes)?).expect("slice is 4 bytes long");
        let dword = u32::from_le_bytes(bytes);
        self.cursor += 4;

        Ok(dword)
    }

    pub fn read_modrm(&mut self) -> Result<Modrm, DecodeError> {
        self.read_byte(Annotation::ModRm).map(Modrm::from)
    }

    fn read_byte_extend_to_word(&mut self, anno: Annotation) -> Result<i16, DecodeError> {
        let byte = self.read_byte(anno)?;
        // Casting the unsigned byte to signed and then casting to i16 will sign extend the byte.
        let byte = byte as i8;

        Ok(byte as i16)
    }

    #[allow(clippy::len_without_is_empty)]
    pub fn len(&self) -> u16 {
        self.cursor as u16
    }
}

/// Annotation describing what a byte being read contextually means.
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum Annotation {
    /// A prefix or opcode is being read.
    PrefixOrOpcode,

    /// A modr/m byte is being read.
    ModRm,

    /// A SIB byte is being read.
    Sib,

    /// A displacement is being read.
    Displacement,

    /// An immediate is being read.
    Immediate,
}

pub struct DecoderTables<'tables> {
    pub byte_1: &'tables [OpcodeInfo; 256],
    pub byte_2: Option<&'tables [OpcodeInfo; 256]>,
}

pub struct Decoder {
    /// Maximum size of an instruction.
    ///
    /// 10 bytes on 186/286, 15 bytes on 386.
    pub max_size: u16,
}

impl Decoder {
    /// Read an opcode and prefixes.
    ///
    /// This function reads in bytes and produces a description of a raw instruction before reading any
    /// operands. This function also validates the prefixes are compatible with the input instructions.
    pub fn read_opcode(
        &self,
        tables: &DecoderTables<'_>,
        buffer: &mut FetchBuffer,
        mode: CodeSize,
    ) -> Result<InstructionRaw, DecodeError> {
        let mut context = DecoderContext::new(mode);
        let raw = loop {
            let byte = buffer.read_byte(Annotation::PrefixOrOpcode)?;
            let table = match context.byte_2() {
                false => Some(tables.byte_1),
                true => tables.byte_2,
            };
            let info = table.ok_or(DecodeError::InvalidOpcode)?[byte as usize];

            match info.ty() {
                Ty::None => return Err(DecodeError::InvalidOpcode),
                Ty::Op(_, _, _) | Ty::Revise(_, _) => {
                    break InstructionRaw {
                        info,
                        address_size: context.address_size(),
                        operand_size: context.operand_size(),
                        lock: context.lock(),
                        segment_override: context.segment_override(),
                        rep: context.rep(),
                    };
                }
                Ty::Prefix => match byte {
                    0x0F => context.set_byte2(),
                    0x26 => context.set_segment_override(Segment::Es),
                    0x2E => context.set_segment_override(Segment::Cs),
                    0x36 => context.set_segment_override(Segment::Ss),
                    0x3E => context.set_segment_override(Segment::Ds),
                    0x64 => context.set_segment_override(Segment::Fs),
                    0x65 => context.set_segment_override(Segment::Gs),
                    0x66 => context.set_operand_size(),
                    0x67 => context.set_address_size(),
                    0xF0 => context.set_lock(),
                    0xF2 => context.set_repne(),
                    0xF3 => context.set_rep_repe(),
                    _ => unreachable!("get_opcode incorrectly returned a prefix"),
                },
            }
        };

        if buffer.len() > self.max_size {
            return Err(DecodeError::TooLong);
        }

        // Verify the instruction is valid with regard to prefixes.
        if !raw.info.lockable() && raw.lock {
            return Err(DecodeError::InvalidPrefix);
        }

        if !raw.info.rep() && raw.rep == Some(RepTy::RepRepe) {
            return Err(DecodeError::InvalidPrefix);
        }

        if !raw.info.repne() && raw.rep == Some(RepTy::Repne) {
            return Err(DecodeError::InvalidPrefix);
        }

        Ok(raw)
    }

    pub fn read_instruction(
        &self,
        tables: &DecoderTables<'_>,
        buffer: &mut FetchBuffer,
        mode: CodeSize,
    ) -> Result<Instruction, DecodeError> {
        let InstructionRaw {
            info,
            address_size,
            operand_size,
            lock,
            segment_override,
            rep,
        } = self.read_opcode(tables, buffer, mode)?;

        // The modrm value is "undefined" if an opcode does not declare it uses the modrm.
        let modrm = if info.uses_modrm() {
            buffer.read_modrm()?
        } else {
            Modrm::from(0)
        };

        let mut ty = info.ty();

        if let Ty::Revise(group, modrm_mask) = ty {
            debug_assert!(info.uses_modrm(), "Opcodes requiring revision must use the modrm");
            ty = revise(group, modrm, modrm_mask)?;
        }

        let Ty::Op(opcode, signature, modrm_mask) = ty else {
            unreachable!();
        };

        // TODO: Reject invalid modrm values (for revised opcodes, this has already happened).
        // TODO: Reject invalid prefixes

        let operands = match signature {
            Signature::Empty => Ok(Operands::None),
            Signature::Eb => eb(buffer, address_size, segment_override, modrm),
            Signature::Ev => todo!(),
            Signature::Ep => todo!(),
            Signature::Eb_Gb => eb_gb(buffer, address_size, segment_override, modrm),
            Signature::Ev_Gv => ev_gv(buffer, address_size, operand_size, segment_override, modrm),
            Signature::Ew_Rw => todo!(),
            Signature::Gb_Eb => todo!(),
            Signature::Gv_Ev => todo!(),
            Signature::Al_Ib => al_ib(buffer),
            Signature::eAx_Iv => eAx_iv(buffer, operand_size),
            Signature::Ib_Al => Ok(Operands::PortAlDx),
            Signature::Iv_eAx => eAx_iv(buffer, operand_size),
            Signature::Es => Ok(Operands::Segment(Segment::Es)),
            Signature::Cs => Ok(Operands::Segment(Segment::Cs)),
            Signature::Ss => Ok(Operands::Segment(Segment::Ss)),
            Signature::Ds => Ok(Operands::Segment(Segment::Ds)),
            Signature::Fs => Ok(Operands::Segment(Segment::Fs)),
            Signature::Gs => Ok(Operands::Segment(Segment::Gs)),
            Signature::Al => Ok(Operands::Register8(Reg8::Al)),
            Signature::Cl => Ok(Operands::Register8(Reg8::Cl)),
            Signature::Dl => Ok(Operands::Register8(Reg8::Dl)),
            Signature::Bl => Ok(Operands::Register8(Reg8::Bl)),
            Signature::Ah => Ok(Operands::Register8(Reg8::Ah)),
            Signature::Ch => Ok(Operands::Register8(Reg8::Ch)),
            Signature::Dh => Ok(Operands::Register8(Reg8::Dh)),
            Signature::Bh => Ok(Operands::Register8(Reg8::Bh)),
            Signature::eAx => reg(signature, operand_size),
            Signature::eCx => reg(signature, operand_size),
            Signature::eDx => reg(signature, operand_size),
            Signature::eBx => reg(signature, operand_size),
            Signature::eSp => reg(signature, operand_size),
            Signature::eBp => reg(signature, operand_size),
            Signature::eSi => reg(signature, operand_size),
            Signature::eDi => reg(signature, operand_size),
            Signature::Ib => ib(buffer),
            Signature::Iv => iv(buffer, operand_size),
            Signature::Gv_Ma => todo!(),
            Signature::Gv_Ev_Iv => todo!(),
            Signature::Gv_Ev_IbExtend => todo!(),
            Signature::Yb_Dx => todo!(),
            Signature::Yv_Dx => todo!(),
            Signature::Dx_Yb => todo!(),
            Signature::Dx_Yv => todo!(),
            Signature::Jb_o => jb(buffer, signature),
            Signature::Jb_no => jb(buffer, signature),
            Signature::Jb_b => jb(buffer, signature),
            Signature::Jb_nb => jb(buffer, signature),
            Signature::Jb_z => jb(buffer, signature),
            Signature::Jb_nz => jb(buffer, signature),
            Signature::Jb_e => jb(buffer, signature),
            Signature::Jb_ne => jb(buffer, signature),
            Signature::Jb_s => jb(buffer, signature),
            Signature::Jb_ns => jb(buffer, signature),
            Signature::Jb_p => jb(buffer, signature),
            Signature::Jb_np => jb(buffer, signature),
            Signature::Jb_l => jb(buffer, signature),
            Signature::Jb_nl => jb(buffer, signature),
            Signature::Jb_le => jb(buffer, signature),
            Signature::Jb_nle => jb(buffer, signature),
            Signature::Eb_Ib => todo!(),
            Signature::Ev_Iv => todo!(),
            Signature::Ev_IbExtend => todo!(),
            Signature::Ew_Sw => todo!(),
            Signature::Sw_Ew => todo!(),
            Signature::Ew => todo!(),
            Signature::Gv_M => todo!(),
            Signature::Ap => ap(buffer, operand_size),
            Signature::Al_Ob => todo!(),
            Signature::eAx_Ov => todo!(),
            Signature::Ob_Al => todo!(),
            Signature::Ov_eAx => todo!(),
            Signature::Xb_Yb => xb_yb(rep),
            Signature::Xv_Yv => xv_yv(rep, operand_size),
            Signature::Yb_Al => todo!(),
            Signature::Yv_eAx => todo!(),
            Signature::Al_Xb => todo!(),
            Signature::eAx_Xv => todo!(),
            Signature::Iw_Ib => iw_ib(buffer),
            Signature::Eb_1 => todo!(),
            Signature::Ev_1 => todo!(),
            Signature::Eb_Cl => todo!(),
            Signature::Ev_Cl => todo!(),
            Signature::Av => todo!(),
            Signature::Al_Dx => Ok(Operands::PortAlDx),
            Signature::eAx_Dx => eAx_dx(operand_size),
            Signature::Dx_Al => todo!(),
            Signature::Dx_eAx => todo!(),
        }?;

        Ok(Instruction { opcode, operands, lock })
    }
}

fn revise(group: Group, modrm: Modrm, mask: ModrmMask) -> Result<Ty, DecodeError> {
    match group {
        Group::Group1Eb_Ib => todo!(),
        Group::Group1Ev_Iv => todo!(),
        Group::Group1Ev_IbExtend => todo!(),
        Group::Group2Gb_Eb => todo!(),
        Group::Group2Gv_Ev => todo!(),
        Group::Group2Eb_1 => todo!(),
        Group::Group2Ev_1 => todo!(),
        Group::Group2Eb_Cl => todo!(),
        Group::Group2Ev_Cl => todo!(),
        Group::Group3Eb => todo!(),
        Group::Group3Ev => todo!(),
        Group::Group4Eb => revise_group4(modrm, mask),
        Group::Group5 => revise_group5(modrm, mask),
    }
}

const GROUP4: [Option<Opcode>; 8] = [Some(Opcode::Inc), Some(Opcode::Dec), None, None, None, None, None, None];

fn revise_group4(modrm: Modrm, mask: ModrmMask) -> Result<Ty, DecodeError> {
    let Some(opcode) = GROUP4[modrm.opcode() as usize] else {
        return Err(DecodeError::InvalidOpcode);
    };

    Ok(Ty::Op(opcode, Signature::Eb, mask))
}

const GROUP5: [Option<(Opcode, Signature)>; 8] = [
    Some((Opcode::Inc, Signature::Ev)),
    Some((Opcode::Dec, Signature::Ev)),
    Some((Opcode::Call, Signature::Ev)),
    Some((Opcode::Call, Signature::Ep)),
    None, // Some((Opcode::Jmp, Signature::Ev)), // TODO: JMP
    None, // Some((Opcode::Jmp, Signature::Ep)),
    Some((Opcode::Push, Signature::Ev)),
    None,
];

fn revise_group5(modrm: Modrm, mask: ModrmMask) -> Result<Ty, DecodeError> {
    let Some((opcode, signature)) = GROUP5[modrm.opcode() as usize] else {
        return Err(DecodeError::InvalidOpcode);
    };

    Ok(Ty::Op(opcode, signature, mask))
}

fn eb(
    buffer: &mut FetchBuffer,
    address_size: CodeSize,
    segment_override: Option<Segment>,
    modrm: Modrm,
) -> Result<Operands, DecodeError> {
    match address_size {
        CodeSize::_16 => eb_a16(buffer, segment_override, modrm),
        CodeSize::_32 => todo!(),
    }
}

fn eb_a16(buffer: &mut FetchBuffer, segment_override: Option<Segment>, modrm: Modrm) -> Result<Operands, DecodeError> {
    let segment = segment_override.unwrap_or(Segment::Ds);

    let operands = match modrm.r#mod() {
        0b00 => Operands::Memory8(segment, mem_a16_mod_00(buffer, modrm)?),
        0b01 => Operands::Memory8(segment, mem_a16_mod_01(buffer, modrm)?),
        0b10 => Operands::Memory8(segment, mem_a16_mod_10(buffer, modrm)?),
        0b11 => Operands::Register8(Reg8::with_reg(modrm)),

        _ => unreachable!(),
    };

    Ok(operands)
}

fn eb_gb(
    buffer: &mut FetchBuffer,
    address_size: CodeSize,
    segment_override: Option<Segment>,
    modrm: Modrm,
) -> Result<Operands, DecodeError> {
    match address_size {
        CodeSize::_16 => eb_gb_a16(buffer, segment_override, modrm),
        CodeSize::_32 => todo!(),
    }
}

fn eb_gb_a16(
    buffer: &mut FetchBuffer,
    segment_override: Option<Segment>,
    modrm: Modrm,
) -> Result<Operands, DecodeError> {
    let segment = segment_override.unwrap_or(Segment::Ds);

    let operands = match modrm.r#mod() {
        0b00 => Operands::MemoryRegister8(segment, mem_a16_mod_00(buffer, modrm)?, Reg8::with_reg(modrm)),
        0b01 => Operands::MemoryRegister8(segment, mem_a16_mod_01(buffer, modrm)?, Reg8::with_reg(modrm)),
        0b10 => Operands::MemoryRegister8(segment, mem_a16_mod_10(buffer, modrm)?, Reg8::with_reg(modrm)),
        0b11 => Operands::RegisterRegister8(Reg8::with_reg(modrm), Reg8::with_rm(modrm)),

        _ => unreachable!(),
    };

    Ok(operands)
}

fn mem_a16_mod_00(buffer: &mut FetchBuffer, modrm: Modrm) -> Result<Memory, DecodeError> {
    let m = match modrm.rm() {
        0b000 => Memory::BxSi(0),
        0b001 => Memory::BxDi(0),
        0b010 => Memory::BpSi(0),
        0b011 => Memory::BpDi(0),
        0b100 => Memory::Si(0),
        0b101 => Memory::Di(0),
        0b110 => Memory::Displacement16(buffer.read_word(Annotation::Displacement)? as i16),
        0b111 => Memory::Bx(0),

        // Since ModRm::rm masks out everything but the three rm bits, this is always unreachable.
        _ => unreachable!(),
    };

    Ok(m)
}

fn mem_a16_mod_01(buffer: &mut FetchBuffer, modrm: Modrm) -> Result<Memory, DecodeError> {
    // Mod == 0b01 always has a disp8 which is sign extended.
    let disp = buffer.read_byte_extend_to_word(Annotation::Displacement)?;

    let m = match modrm.rm() {
        0b000 => Memory::BxSi(disp),
        0b001 => Memory::BxDi(disp),
        0b010 => Memory::BpSi(disp),
        0b011 => Memory::BpDi(disp),
        0b100 => Memory::Si(disp),
        0b101 => Memory::Di(disp),
        0b110 => Memory::Bp(disp),
        0b111 => Memory::Bx(disp),

        // Since ModRm::rm masks out everything but the three rm bits, this is always unreachable.
        _ => unreachable!(),
    };

    Ok(m)
}

fn mem_a16_mod_10(buffer: &mut FetchBuffer, modrm: Modrm) -> Result<Memory, DecodeError> {
    // Mod == 0b10 is always an i16 value
    let disp = buffer.read_word(Annotation::Displacement)? as i16;

    let m = match modrm.rm() {
        0b000 => Memory::BxSi(disp),
        0b001 => Memory::BxDi(disp),
        0b010 => Memory::BpSi(disp),
        0b011 => Memory::BpDi(disp),
        0b100 => Memory::Si(disp),
        0b101 => Memory::Di(disp),
        0b110 => Memory::Bp(disp),
        0b111 => Memory::Bx(disp),

        // Since ModRm::rm masks out everything but the three rm bits, this is always unreachable.
        _ => unreachable!(),
    };

    Ok(m)
}

fn ev_gv(
    buffer: &mut FetchBuffer,
    address_size: CodeSize,
    operand_size: CodeSize,
    segment_override: Option<Segment>,
    modrm: Modrm,
) -> Result<Operands, DecodeError> {
    match (operand_size, address_size) {
        (CodeSize::_16, CodeSize::_16) => ew_gw_a16(buffer, segment_override, modrm),
        (CodeSize::_16, CodeSize::_32) => todo!(),
        (CodeSize::_32, CodeSize::_16) => ed_gd_a16(buffer, segment_override, modrm),
        (CodeSize::_32, CodeSize::_32) => todo!(),
    }
}

fn ew_gw_a16(
    buffer: &mut FetchBuffer,
    segment_override: Option<Segment>,
    modrm: Modrm,
) -> Result<Operands, DecodeError> {
    let segment = segment_override.unwrap_or(Segment::Ds);

    let operands = match modrm.r#mod() {
        0b00 => Operands::MemoryRegister16(segment, mem_a16_mod_00(buffer, modrm)?, Reg16::with_reg(modrm)),
        0b01 => Operands::MemoryRegister16(segment, mem_a16_mod_01(buffer, modrm)?, Reg16::with_reg(modrm)),
        0b10 => Operands::MemoryRegister16(segment, mem_a16_mod_10(buffer, modrm)?, Reg16::with_reg(modrm)),
        0b11 => Operands::RegisterRegister16(Reg16::with_reg(modrm), Reg16::with_rm(modrm)),

        _ => unreachable!(),
    };

    Ok(operands)
}

fn ed_gd_a16(
    buffer: &mut FetchBuffer,
    segment_override: Option<Segment>,
    modrm: Modrm,
) -> Result<Operands, DecodeError> {
    let segment = segment_override.unwrap_or(Segment::Ds);

    let operands = match modrm.r#mod() {
        0b00 => Operands::MemoryRegister32(segment, mem_a16_mod_00(buffer, modrm)?, Reg32::with_reg(modrm)),
        0b01 => Operands::MemoryRegister32(segment, mem_a16_mod_01(buffer, modrm)?, Reg32::with_reg(modrm)),
        0b10 => Operands::MemoryRegister32(segment, mem_a16_mod_10(buffer, modrm)?, Reg32::with_reg(modrm)),
        0b11 => Operands::RegisterRegister32(Reg32::with_reg(modrm), Reg32::with_rm(modrm)),

        _ => unreachable!(),
    };

    Ok(operands)
}

fn al_ib(buffer: &mut FetchBuffer) -> Result<Operands, DecodeError> {
    let imm = buffer.read_byte(Annotation::Immediate)?;
    Ok(Operands::RegisterImmediate8(Reg8::Al, imm))
}

#[allow(non_snake_case)]
fn eAx_iv(buffer: &mut FetchBuffer, operand_size: CodeSize) -> Result<Operands, DecodeError> {
    let operands = match operand_size {
        CodeSize::_16 => Operands::RegisterImmediate16(Reg16::Ax, buffer.read_word(Annotation::Immediate)?),
        CodeSize::_32 => Operands::RegisterImmediate32(Reg32::Eax, buffer.read_dword(Annotation::Immediate)?),
    };

    Ok(operands)
}

fn reg(signature: Signature, operand_size: CodeSize) -> Result<Operands, DecodeError> {
    let operands = match (signature, operand_size) {
        (Signature::eAx, CodeSize::_16) => Operands::Register16(Reg16::Ax),
        (Signature::eAx, CodeSize::_32) => Operands::Register32(Reg32::Eax),
        (Signature::eCx, CodeSize::_16) => Operands::Register16(Reg16::Cx),
        (Signature::eCx, CodeSize::_32) => Operands::Register32(Reg32::Ecx),
        (Signature::eDx, CodeSize::_16) => Operands::Register16(Reg16::Dx),
        (Signature::eDx, CodeSize::_32) => Operands::Register32(Reg32::Edx),
        (Signature::eBx, CodeSize::_16) => Operands::Register16(Reg16::Bx),
        (Signature::eBx, CodeSize::_32) => Operands::Register32(Reg32::Ebx),
        (Signature::eSp, CodeSize::_16) => Operands::Register16(Reg16::Sp),
        (Signature::eSp, CodeSize::_32) => Operands::Register32(Reg32::Esp),
        (Signature::eBp, CodeSize::_16) => Operands::Register16(Reg16::Bp),
        (Signature::eBp, CodeSize::_32) => Operands::Register32(Reg32::Ebp),
        (Signature::eSi, CodeSize::_16) => Operands::Register16(Reg16::Si),
        (Signature::eSi, CodeSize::_32) => Operands::Register32(Reg32::Esi),
        (Signature::eDi, CodeSize::_16) => Operands::Register16(Reg16::Di),
        (Signature::eDi, CodeSize::_32) => Operands::Register32(Reg32::Edi),
        _ => unreachable!(),
    };

    Ok(operands)
}

fn ib(buffer: &mut FetchBuffer) -> Result<Operands, DecodeError> {
    buffer.read_byte(Annotation::Immediate).map(Operands::Immediate8)
}

fn iv(buffer: &mut FetchBuffer, operand_size: CodeSize) -> Result<Operands, DecodeError> {
    match operand_size {
        CodeSize::_16 => buffer.read_word(Annotation::Immediate).map(Operands::Immediate16),
        CodeSize::_32 => buffer.read_dword(Annotation::Immediate).map(Operands::Immediate32),
    }
}

fn jb(buffer: &mut FetchBuffer, signature: Signature) -> Result<Operands, DecodeError> {
    let displacement = buffer.read_byte(Annotation::Displacement)?;

    let cond = match signature {
        Signature::Jb_o => Cond::O,
        Signature::Jb_no => Cond::No,
        Signature::Jb_b => Cond::B,
        Signature::Jb_nb => Cond::Nb,
        Signature::Jb_z => Cond::Z,
        Signature::Jb_nz => Cond::Nz,
        Signature::Jb_e => Cond::E,
        Signature::Jb_ne => Cond::Ne,
        Signature::Jb_s => Cond::S,
        Signature::Jb_ns => Cond::Ns,
        Signature::Jb_p => Cond::P,
        Signature::Jb_np => Cond::Np,
        Signature::Jb_l => Cond::L,
        Signature::Jb_nl => Cond::Nl,
        Signature::Jb_le => Cond::Le,
        Signature::Jb_nle => Cond::Nle,
        _ => unreachable!(),
    };

    Ok(Operands::ConditionDisplacement8(cond, displacement as i8))
}

fn ap(buffer: &mut FetchBuffer, operand_size: CodeSize) -> Result<Operands, DecodeError> {
    match operand_size {
        CodeSize::_16 => {
            let displacement = buffer.read_word(Annotation::Displacement)?;
            let segment = buffer.read_word(Annotation::Displacement)?;

            Ok(Operands::DirectAddress16 { segment, displacement })
        }
        CodeSize::_32 => todo!(),
    }
}

fn xb_yb(rep: Option<RepTy>) -> Result<Operands, DecodeError> {
    Ok(Operands::CompareMoveMemory8(rep))
}

fn xv_yv(rep: Option<RepTy>, operand_size: CodeSize) -> Result<Operands, DecodeError> {
    let operands = match operand_size {
        CodeSize::_16 => Operands::CompareMoveMemory16(rep),
        CodeSize::_32 => Operands::CompareMoveMemory32(rep),
    };

    Ok(operands)
}

fn iw_ib(buffer: &mut FetchBuffer) -> Result<Operands, DecodeError> {
    let frame_size = buffer.read_word(Annotation::Immediate)?;
    let nesting = buffer.read_byte(Annotation::Immediate)?;

    Ok(Operands::Frame(frame_size, nesting))
}

#[allow(non_snake_case)]
fn eAx_dx(operand_size: CodeSize) -> Result<Operands, DecodeError> {
    let operands = match operand_size {
        CodeSize::_16 => Operands::PortAxDx,
        CodeSize::_32 => Operands::PortEaxDx,
    };

    Ok(operands)
}

struct DecoderContext {
    mode: CodeSize,
    address_size: CodeSize,
    operand_size: CodeSize,
    byte_2: bool,
    lock: bool,
    segment: Option<Segment>,
    rep: Option<RepTy>,
}

impl DecoderContext {
    fn new(mode: CodeSize) -> Self {
        Self {
            mode,
            address_size: mode,
            operand_size: mode,
            byte_2: false,
            lock: false,
            segment: None,
            rep: None,
        }
    }

    fn set_operand_size(&mut self) {
        self.operand_size = !self.mode;
    }

    fn set_address_size(&mut self) {
        self.address_size = !self.mode;
    }

    fn set_byte2(&mut self) {
        self.byte_2 = true;
    }

    fn set_lock(&mut self) {
        self.lock = true;
    }

    fn set_segment_override(&mut self, segment: Segment) {
        self.segment = Some(segment);
    }

    fn set_repne(&mut self) {
        self.rep = Some(RepTy::Repne);
    }

    fn set_rep_repe(&mut self) {
        self.rep = Some(RepTy::RepRepe);
    }

    fn operand_size(&self) -> CodeSize {
        self.operand_size
    }

    fn address_size(&self) -> CodeSize {
        self.address_size
    }

    fn lock(&self) -> bool {
        self.lock
    }

    fn segment_override(&self) -> Option<Segment> {
        self.segment
    }

    fn rep(&self) -> Option<RepTy> {
        self.rep
    }

    fn byte_2(&self) -> bool {
        self.byte_2
    }
}
