use std::{env, fs, io, path::PathBuf};

use libtest_mimic::{Arguments, Trial};

fn main() {
    test_environment();

    let args = Arguments::from_args();
    let tests = vec![];

    libtest_mimic::run(&args, tests).exit();
}

fn test_environment() {
    let mut current_dir = env::current_dir().unwrap();
    current_dir.pop();
    current_dir.pop();

    current_dir.extend(["submodules", "singlestep"]);

    let submodules = fs::read_dir(current_dir)
        .unwrap()
        .into_iter()
        .filter(|entry| {
            if let Ok(entry) = entry {
                if let Ok(ty) = entry.file_type() {
                    return ty.is_dir();
                }
            }

            // Propagate errors
            true
        })
        .map(|result| result.map(|entry| entry.path()))
        .collect::<io::Result<Vec<PathBuf>>>()
        .unwrap();

    let mut empty_submodules = Vec::new();

    for submodule in submodules {
        let read = fs::read_dir(&submodule).unwrap();

        if read.into_iter().count() == 0 {
            empty_submodules.push(submodule);
        }
    }

    if !empty_submodules.is_empty() {
        panic!("Some submodules were not cloned. run `git submodule update --init --recursive` to clone these")
    }
}
