#![no_std]
#![deny(clippy::disallowed_methods)]

mod arithmetic;
pub use arithmetic::*;
mod interpret;
pub use interpret::*;
mod register;
pub use register::*;

/// Returns the parity value for the value.
///
/// The Intel SDM states that the value should be the least significant byte of a result.
pub const fn parity(value: u8) -> bool {
    PARITY[value as usize]
}

/// A lookup table is used for parity calculations since u8::count_ones will generate a huge amount of code.
const PARITY: [bool; 256] = make_lut();

const fn make_lut() -> [bool; 256] {
    let mut lut = [false; 256];
    let mut index = 0usize;

    // for is not const yet.
    loop {
        if index >= lut.len() {
            break;
        }

        // Quoting from the Intel SDM:
        //
        //     Set if the least-significant byte of the result contains an even number of 1 bits; cleared
        //     otherwise.
        lut[index] = (index as u8).count_ones() % 2 == 0;
        index += 1;
    }

    lut
}
