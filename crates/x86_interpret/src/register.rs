/// Extract the word of a 32-bit value.
#[inline(always)]
pub const fn word(x: u32) -> u16 {
    let tmp = x & 0x0000_FFFF;
    tmp as u16
}

/// Set the value of a word of a 32-bit value.
#[inline(always)]
pub const fn set_word(current: u32, set: u16) -> u32 {
    let mut tmp = current & 0xFFFF_0000;
    tmp |= set as u32;
    tmp
}

/// Extract the high byte of a 32-bit value.
#[inline(always)]
pub const fn high_byte(x: u32) -> u8 {
    let mut tmp = x & 0x0000_FF00;
    tmp >>= 8;
    tmp as u8
}

/// Set the value of the high byte of a 32-bit value.
#[inline(always)]
pub const fn set_high_byte(current: u32, set: u8) -> u32 {
    let mut tmp = current & 0xFFFF_00FF;
    tmp |= (set as u32) << 8;
    tmp
}

/// Extract the low byte of a 32-bit value.
#[inline(always)]
pub const fn low_byte(x: u32) -> u8 {
    (x & 0x0000_00FF) as u8
}

/// Set the value of the low byte of a 32-bit value.
#[inline(always)]
pub const fn set_low_byte(current: u32, set: u8) -> u32 {
    let mut tmp = current & 0xFFFF_FF00;
    tmp |= set as u32;
    tmp
}

#[cfg(test)]
mod tests {
    use crate::{high_byte, low_byte, set_high_byte, set_low_byte, set_word, word};

    /// Tests that accessing and mutating parts of the register is done correctly.
    #[test]
    fn reg_parts() {
        let value = 0xFFFF_AABB_u32;

        let word = word(value);
        assert_eq!(word, 0xAABB);

        let high_b = high_byte(value);
        assert_eq!(high_b, 0xAA);

        let low_b = low_byte(value);
        assert_eq!(low_b, 0xBB);

        let value = set_low_byte(value, 0x00);
        assert_eq!(value, 0xFFFF_AA00_u32);

        // TODO: Possible rustc lint improvement
        // Remove the semicolon and watch rustc struggle to realize the fix is a semicolon to make the borrow a
        // statement. But rustc thinks we are trying to multiply.
        let value = set_high_byte(value, 0xEE);
        assert_eq!(value, 0xFFFF_EE00_u32);

        let value = set_word(value, 0x7777);
        assert_eq!(value, 0xFFFF_7777_u32);
    }
}
