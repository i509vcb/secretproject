use x86_decode::{Decoder, DecoderTables, FetchBuffer};
use x86_types::{CodeSize, Reg16, Reg32, Reg8, Segment};

pub trait Bus {
    fn byte_read(&mut self, address: u32) -> u8;

    fn byte_write(&mut self, address: u32, value: u8);

    fn word_read(&mut self, address: u32) -> u16;

    fn word_write(&mut self, address: u32);

    fn dword_read(&mut self, address: u32) -> u32;

    fn dword_write(&mut self, address: u32);

    fn io_byte_read(&mut self, address: u16) -> u8;

    fn io_byte_write(&mut self, address: u16, value: u8);

    fn io_word_read(&mut self, address: u16) -> u16;

    fn io_word_write(&mut self, address: u16, value: u16);

    fn io_dword_read(&mut self, address: u16) -> u32;

    fn io_dword_write(&mut self, address: u16, value: u32);

    fn fetch(&mut self, address: u32) -> FetchBuffer {
        // Not great... Not terrible.
        let mut buffer = [0u8; 15];

        for offset in 0..buffer.len() {
            buffer[offset] = self.byte_read(address + offset as u32);
        }

        FetchBuffer::new(buffer)
    }
}

pub struct CpuState {
    pub eax: u32,
    pub ecx: u32,
    pub edx: u32,
    pub ebx: u32,
    pub esp: u32,
    pub ebp: u32,
    pub esi: u32,
    pub edi: u32,
    pub es: u16,
    pub cs: u16,
    pub ss: u16,
    pub ds: u16,
    pub fs: u16,
    pub gs: u16,
    // TODO: This does not need to be packed, can pack when needed for pushf/popf/sahf/lahf.
    pub eflags: u32,
    pub eip: u32,
}

impl CpuState {
    // TODO: Register accessors
    pub fn get_reg8(&self, register: Reg8) -> u8 {
        let value = match register {
            Reg8::Al => self.eax,
            Reg8::Cl => self.ecx,
            Reg8::Dl => self.edx,
            Reg8::Bl => self.ebx,
            Reg8::Ah => (self.eax >> 8) & 0x0000_00FF,
            Reg8::Ch => (self.ecx >> 8) & 0x0000_00FF,
            Reg8::Dh => (self.edx >> 8) & 0x0000_00FF,
            Reg8::Bh => (self.ebx >> 8) & 0x0000_00FF,
            Reg8::Bpl => self.esp,
            Reg8::Spl => self.ebp,
            Reg8::Dil => self.esi,
            Reg8::Sil => self.edi,
        };

        value as u8
    }

    pub fn set_reg8(&mut self, register: Reg8, value: u8) {
        let dst = match register {
            Reg8::Al => &mut self.eax,
            Reg8::Cl => &mut self.ecx,
            Reg8::Dl => &mut self.edx,
            Reg8::Bl => &mut self.ebx,
            Reg8::Ah => &mut self.eax,
            Reg8::Ch => &mut self.ecx,
            Reg8::Dh => &mut self.edx,
            Reg8::Bh => &mut self.ebx,
            Reg8::Spl => &mut self.esp,
            Reg8::Bpl => &mut self.ebp,
            Reg8::Sil => &mut self.esi,
            Reg8::Dil => &mut self.edi,
        };

        if matches!(register, Reg8::Ah | Reg8::Ch | Reg8::Dh | Reg8::Bh) {
            *dst &= 0xFFFF_00FF;
            *dst |= (value as u32) << 8;
        } else {
            *dst &= 0xFFFF_FF00;
            *dst |= value as u32;
        }
    }

    pub fn get_reg16(&self, register: Reg16) -> u16 {
        let value = match register {
            Reg16::Ax => self.eax,
            Reg16::Cx => self.ecx,
            Reg16::Dx => self.edx,
            Reg16::Bx => self.ebx,
            Reg16::Sp => self.esp,
            Reg16::Bp => self.ebp,
            Reg16::Si => self.esi,
            Reg16::Di => self.edi,
        };

        value as u16
    }

    pub fn set_reg16(&mut self, register: Reg16, value: u16) {
        let register = match register {
            Reg16::Ax => &mut self.eax,
            Reg16::Cx => &mut self.ecx,
            Reg16::Dx => &mut self.edx,
            Reg16::Bx => &mut self.ebx,
            Reg16::Sp => &mut self.esp,
            Reg16::Bp => &mut self.ebp,
            Reg16::Si => &mut self.esi,
            Reg16::Di => &mut self.edi,
        };

        *register &= 0xFFFF_0000;
        *register |= value as u32;
    }

    pub fn get_reg32(&self, register: Reg32) -> u32 {
        match register {
            Reg32::Eax => self.eax,
            Reg32::Ecx => self.ecx,
            Reg32::Edx => self.edx,
            Reg32::Ebx => self.ebx,
            Reg32::Esp => self.esp,
            Reg32::Ebp => self.ebp,
            Reg32::Esi => self.esi,
            Reg32::Edi => self.edi,
        }
    }

    pub fn set_reg32(&mut self, register: Reg32, value: u32) {
        let reg = match register {
            Reg32::Eax => &mut self.eax,
            Reg32::Ecx => &mut self.ecx,
            Reg32::Edx => &mut self.edx,
            Reg32::Ebx => &mut self.ebx,
            Reg32::Esp => &mut self.esp,
            Reg32::Ebp => &mut self.ebp,
            Reg32::Esi => &mut self.esi,
            Reg32::Edi => &mut self.edi,
        };

        *reg = value;
    }

    pub fn get_segment(&self, segment: Segment) -> u16 {
        match segment {
            Segment::Es => self.es,
            Segment::Cs => self.cs,
            Segment::Ss => self.ss,
            Segment::Ds => self.ds,
            Segment::Fs => self.fs,
            Segment::Gs => self.gs,
        }
    }

    pub fn set_segment(&mut self, segment: Segment, value: u16) {
        let segment = match segment {
            Segment::Es => &mut self.es,
            Segment::Cs => &mut self.cs,
            Segment::Ss => &mut self.ss,
            Segment::Ds => &mut self.ds,
            Segment::Fs => &mut self.fs,
            Segment::Gs => &mut self.gs,
        };

        *segment = value;
    }

    pub fn get_eflags(&self) -> u32 {
        todo!()
    }

    fn get_fetch_address(&self) -> u32 {
        // TODO: Protected mode

        let segment = self.cs as u32;
        let displacement = self.eip & 0x0000_FFFF;
        let address = segment << 4 & displacement;

        address
    }

    // TODO: Return value for things
    // Although do we need one? Exceptions are raised on CPU only, but a triple fault could force a reset signal then.
    // Also for debugging the host may want to be able to query the state after an exception is produced.
    pub fn interpret<Bus: self::Bus>(&mut self, bus: &mut Bus, decoder: &Decoder, tables: &DecoderTables<'_>) {
        // TODO: Handle protected mode.

        let address = self.get_fetch_address();
        let mut buffer = bus.fetch(address);

        // TODO: Consider mode
        let Ok(inst) = decoder.read_instruction(tables, &mut buffer, CodeSize::_16) else {
            todo!("Handle exceptions")
        };
    }
}

#[cfg(test)]
mod tests {
    use crate::Bus;

    #[test]
    fn is_bus_object_safe() {
        #[allow(dead_code)]
        fn object_safe(_b: &mut dyn Bus) {}
    }
}
