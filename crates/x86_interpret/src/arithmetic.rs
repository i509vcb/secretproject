// Explicitly handle math that could overflow
#[deny(clippy::arithmetic_side_effects)]

/// ALU result of an arithmetic operation.
#[derive(Debug, Clone, Copy)]
pub struct AluResult<T> {
    /// The result of the operation.
    pub result: T,

    /// Whether the operation generated a carry or borrow.
    pub carry: bool,

    /// Whether there was signed overflow.
    pub overflow: bool,

    /// Whether the sign of the result is negative.
    pub sign: bool,

    pub zero: bool,

    /// Whether bit 3 generated a carry or borrow.
    pub aux_carry: bool,
}

/// Arithmetic operations and calculating flags.
pub trait X86Arithmetic: Sized {
    /// Calculate `self + rhs + carry`.
    ///
    /// This will return the following:
    /// - The result of the addition
    /// - Whether the addition generated a carry
    /// - Whether the addition generated an auxillary carry.
    ///
    /// An auxillary carry is when bit 3 generates a carry.
    ///
    /// ```
    /// use x86_interpret::X86Arithmetic;
    ///
    /// assert!(0b0000_1111_u8.x86_adc(1u8, false).aux_carry);
    /// assert_eq!(0b0000_1111_u8 + 1u8, 0b0001_0000);
    /// ```
    fn x86_adc(self, rhs: Self, carry: bool) -> AluResult<Self>;

    fn x86_add(self, rhs: Self) -> AluResult<Self> {
        self.x86_adc(rhs, false)
    }

    /// Calculate `self - rhs - borrow`.
    ///
    /// This will return the following:
    /// - The result of the subtraction
    /// - Whether the subtraction generated a borrow
    /// - Whether the addition generated an auxillary borrow.
    ///
    /// An auxillary carry is when bit 3 generates a borrow.
    ///
    /// ```
    /// use x86_interpret::X86Arithmetic;
    ///
    /// assert!(0b1111_0000_u8.x86_sbb(1u8, false).aux_carry);
    /// assert_eq!(0b0000_1111_u8 + 1u8, 0b0001_0000);
    /// assert_eq!(0b1111_0000_u8 - 1u8, 0b1110_1111);
    /// ```
    fn x86_sbb(self, rhs: Self, borrow: bool) -> AluResult<Self>;

    fn x86_sub(self, rhs: Self) -> AluResult<Self> {
        self.x86_sbb(rhs, false)
    }

    /// Calculate the 2s complement negation of this value.
    ///
    /// In 2s complement, a number has the following range: `-2^(n - 1)` to `2^(n - 1) - 1` where `n` is the number
    /// of bits the number is represented with.
    ///
    /// Given the range in 2s complement, this means the most negative value that can be represented has no
    /// positive equivalent. This means that when the twos complement negation of the most negative value is
    /// taken, there will be overflow.
    ///
    /// ```
    /// use x86_interpret::X86Arithmetic;
    ///
    /// // When taking a 2s complement negation, if the value is the MIN value then there is an overflow.
    /// assert!((i8::MIN as u8).x86_neg().overflow);
    ///
    /// // Otherwise overflow isn't possible if the value is not the most negative value (i8::MIN).
    /// assert!(!((127_i8 as u8).x86_neg().overflow));
    /// assert!(!((43_i8 as u8).x86_neg().overflow));
    /// ```
    fn x86_neg(self) -> AluResult<Self>;

    /// Calculate the logical AND of this value.
    ///
    /// The `AF` flag is undefined after this call, so the [`AluResult::aux_carry`] flag should be ignored.
    fn x86_and(self, rhs: Self) -> AluResult<Self>;

    fn x86_xor(self, rhs: Self) -> AluResult<Self>;
}

macro_rules! impl_x86_arith {
    ($($ty: ty where Neg = $neg_ty: ty),*) => {
        $(
            impl X86Arithmetic for $ty {
                fn x86_adc(self, rhs: Self, carry: bool) -> AluResult<Self> {
                    // TODO: use carrying_add when stabilized: https://github.com/rust-lang/rust/issues/85532
                    let (tmp, carry_a) = self.overflowing_add(rhs);
                    let (result, carry_b) = tmp.overflowing_add(carry.into());
                    // Check if signed overflow occurred (was the MSB toggled on).
                    let overflow = (self ^ rhs) & (self ^ result) & (1 << (<$ty>::BITS - 1)) != 0;
                    let aux_carry = ((self ^ rhs) ^ result) & 0x10 != 0;
                    let sign = (result as $neg_ty).is_negative();
                    let zero = result == 0;

                    AluResult {
                        result,
                        overflow,
                        carry: carry_a | carry_b,
                        sign,
                        zero,
                        aux_carry,
                    }
                }

                fn x86_sbb(self, rhs: Self, borrow: bool) -> AluResult<Self> {
                    // TODO: use borrowing_sub when stabilized: https://github.com/rust-lang/rust/issues/85532
                    let (tmp, borrow_a) = self.overflowing_sub(rhs);
                    let (result, borrow_b) = tmp.overflowing_sub(borrow.into());
                    // Check if signed overflow occurred (was the MSB toggled on).
                    let overflow = (self ^ rhs) & (self ^ result) & (1 << (<$ty>::BITS - 1)) != 0;
                    let aux_carry = ((self ^ rhs) ^ result) & 0x10 != 0;
                    let sign = (result as $neg_ty).is_negative();
                    let zero = result == 0;

                    AluResult {
                        result,
                        overflow,
                        carry: borrow_a | borrow_b,
                        sign,
                        zero,
                        aux_carry,
                    }
                }

                fn x86_neg(self) -> AluResult<Self> {
                    // wrapping_neg effectively takes the 2s complement negation of the value.
                    let result = self.wrapping_neg();
                    // Intel SDM: The CF flag set to 0 if the source operand is 0; otherwise it is set to 1
                    let carry = self == 0;
                    let sign = (self as $neg_ty).is_negative();
                    let overflow = self == <$neg_ty>::MIN as Self;
                    let aux_carry = (self ^ result) & 0x10 != 0;
                    let zero = result == 0;

                    AluResult {
                        result,
                        overflow,
                        carry,
                        sign,
                        zero,
                        aux_carry,
                    }
                }

                fn x86_and(self, rhs: Self) -> AluResult<Self> {
                    let result = self & rhs;
                    let sign = (result as $neg_ty).is_negative();
                    let zero = result == 0;

                    // Intel SDM: The OF and CF flags are cleared.
                    AluResult {
                        result,
                        overflow: false,
                        carry: false,
                        sign,
                        zero,
                        // AF is undefined
                        aux_carry: false,
                    }
                }

                fn x86_xor(self, rhs: Self) -> AluResult<Self> {
                    let result = self ^ rhs;
                    let sign = (result as $neg_ty).is_negative();
                    let zero = result == 0;

                    // Intel SDM: The OF and CF flags are cleared.
                    AluResult {
                        result,
                        overflow: false,
                        carry: false,
                        sign,
                        zero,
                        // AF is undefined
                        aux_carry: false,
                    }
                }
            }
        )*
    };
}

impl_x86_arith! [
    u8 where Neg = i8,
    u16 where Neg = i16,
    u32 where Neg = i32
];
