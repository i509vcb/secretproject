#![no_std]
// The BitFlags trait calls a disallowed method
#![allow(clippy::disallowed_methods)]
#![deny(clippy::arithmetic_side_effects)]

use core::ops::Not;

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum Isa {
    _8086,
    V30,
    _186,
    _286,
    _386,
    _486,
}

impl Isa {
    pub const fn is_8086(self) -> bool {
        matches!(
            self,
            Self::_8086 | Self::V30 | Self::_186 | Self::_286 | Self::_386 | Self::_486
        )
    }

    pub const fn is_v30(self) -> bool {
        matches!(self, Self::V30)
    }

    pub const fn is_186(self) -> bool {
        matches!(self, Self::_186 | Self::_286 | Self::_386 | Self::_486)
    }

    pub const fn is_286(self) -> bool {
        matches!(self, Self::_286 | Self::_386 | Self::_486)
    }

    pub const fn is_386(self) -> bool {
        matches!(self, Self::_386 | Self::_486)
    }

    pub const fn is_486(self) -> bool {
        matches!(self, Self::_486)
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum RepTy {
    Repne,
    RepRepe,
}

#[derive(Debug, Default, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub enum CodeSize {
    #[default]
    _16,
    _32,
}

impl CodeSize {
    pub const fn flip(self) -> Self {
        match self {
            CodeSize::_16 => CodeSize::_32,
            CodeSize::_32 => CodeSize::_16,
        }
    }
}

impl Not for CodeSize {
    type Output = Self;

    fn not(self) -> Self::Output {
        self.flip()
    }
}

/// Byte general purpose register
#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub enum Reg8 {
    Al,
    Cl,
    Dl,
    Bl,
    Ah,
    Ch,
    Dh,
    Bh,
    Bpl,
    Spl,
    Dil,
    Sil,
}

impl Reg8 {
    /// Returns the byte register corresponding to the register field for an operand of `r8`.
    pub const fn from_reg_field(value: u8) -> Option<Self> {
        let reg = match value {
            0 => Self::Al,
            1 => Self::Cl,
            2 => Self::Dl,
            3 => Self::Bl,
            4 => Self::Ah,
            5 => Self::Ch,
            6 => Self::Dh,
            7 => Self::Bh,
            _ => return None,
        };

        Some(reg)
    }

    pub fn with_rm(modrm: Modrm) -> Self {
        // None is always unreachable
        Self::from_reg_field(modrm.rm()).expect("unreachable")
    }

    pub fn with_reg(modrm: Modrm) -> Self {
        // None is always unreachable
        Self::from_reg_field(modrm.reg()).expect("unreachable")
    }
}

/// Word general purpose register
#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub enum Reg16 {
    Ax,
    Cx,
    Dx,
    Bx,
    Sp,
    Bp,
    Si,
    Di,
}

impl Reg16 {
    /// Returns the byte register corresponding to the register field for an operand of `r16`.
    pub const fn from_reg_field(value: u8) -> Option<Self> {
        let reg = match value {
            0 => Self::Ax,
            1 => Self::Cx,
            2 => Self::Dx,
            3 => Self::Bx,
            4 => Self::Sp,
            5 => Self::Bp,
            6 => Self::Si,
            7 => Self::Di,
            _ => return None,
        };

        Some(reg)
    }

    pub fn with_rm(modrm: Modrm) -> Self {
        // None is always unreachable
        Self::from_reg_field(modrm.rm()).expect("unreachable")
    }

    pub fn with_reg(modrm: Modrm) -> Self {
        // None is always unreachable
        Self::from_reg_field(modrm.reg()).expect("unreachable")
    }
}

impl From<Reg32> for Reg16 {
    fn from(value: Reg32) -> Self {
        match value {
            Reg32::Eax => Reg16::Ax,
            Reg32::Ecx => Reg16::Cx,
            Reg32::Edx => Reg16::Dx,
            Reg32::Ebx => Reg16::Bx,
            Reg32::Esp => Reg16::Sp,
            Reg32::Ebp => Reg16::Bp,
            Reg32::Esi => Reg16::Si,
            Reg32::Edi => Reg16::Di,
        }
    }
}

/// Double word general purpose register
#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub enum Reg32 {
    Eax,
    Ecx,
    Edx,
    Ebx,
    Esp,
    Ebp,
    Esi,
    Edi,
}

impl Reg32 {
    /// Returns the byte register corresponding to the register field for an operand of `r32`.
    pub const fn from_reg_field(value: u8) -> Option<Self> {
        let reg = match value {
            0 => Self::Eax,
            1 => Self::Ecx,
            2 => Self::Edx,
            3 => Self::Ebx,
            4 => Self::Esp,
            5 => Self::Ebp,
            6 => Self::Esi,
            7 => Self::Edi,
            _ => return None,
        };

        Some(reg)
    }

    pub fn with_rm(modrm: Modrm) -> Self {
        // None is always unreachable
        Self::from_reg_field(modrm.rm()).expect("unreachable")
    }

    pub fn with_reg(modrm: Modrm) -> Self {
        // None is always unreachable
        Self::from_reg_field(modrm.reg()).expect("unreachable")
    }
}

impl From<Reg16> for Reg32 {
    fn from(value: Reg16) -> Self {
        match value {
            Reg16::Ax => Reg32::Eax,
            Reg16::Cx => Reg32::Ecx,
            Reg16::Dx => Reg32::Edx,
            Reg16::Bx => Reg32::Ebx,
            Reg16::Sp => Reg32::Esp,
            Reg16::Bp => Reg32::Ebp,
            Reg16::Si => Reg32::Esi,
            Reg16::Di => Reg32::Edi,
        }
    }
}

/// Segment register
#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub enum Segment {
    Es,
    Cs,
    Ss,
    Ds,
    Fs,
    Gs,
}

bitflags::bitflags! {
    /// The x86 flags register.
    #[derive(Debug, Clone, Copy)]
    pub struct Flags: u32 {
        /// Carry Flag
        const CF = 1 << 0;

        // Reserved (1) = 0x02

        /// Parity flag
        const PF = 1 << 2;

        // Reserved (0) = 0x08

        /// Adjust flag
        const AF = 1 << 4;

        // Reserved (0) = 0x20

        /// Zero flag
        const ZF = 1 << 6;

        /// Sign flag
        const SF = 1 << 7;

        /// Trap flag
        const TF = 1 << 8;

        /// Interrupt flag
        const IF = 1 << 9;

        /// Direction flag
        const DF = 1 << 10;

        /// Overflow flag
        const OF = 1 << 11;

        /// IOPL lower bit
        const IOPL_0 = 1 << 12;

        /// IOPL upper bit
        const IOPL_1 = 1 << 13;

        /// Nested task flag
        const NT = 1 << 14;

        // Reserved (0) = 0x8000

        /// Resume flag
        const RF = 1 << 16;

        /// Virtual-8086 mode
        const VM = 1 << 17;

        /// Alignment check
        const AC = 1 << 18;

        /// Virtual interrupt flag
        const VIF = 1 << 19;

        /// Virtual interrupt pending flag
        const VIP = 1 << 20;

        /// Identification flag
        const ID = 1 << 21;

        // Reserved (0) = 0x400000 - 0x80000000
    }
}

impl Flags {
    pub const fn get_iopl(self) -> u8 {
        let bits = self.bits();
        // Mask the MSB and LSB IOPL bits.
        let bits = bits & (Self::IOPL_0.bits() | Self::IOPL_1.bits());
        // Shift to get the 2-bit IOPL flag value
        let iopl = (bits >> 12) as u8;

        debug_assert!(iopl <= 3, "IOPL flag will never have a value greater than 3 (0b11)");
        iopl
    }

    /// Returns the result of the LAHF instruction from the flags.
    ///
    /// ```
    /// use x86_types::Flags;
    ///
    /// // LAHF forces bits 1, 3 and 5 to be set to 1, 0 and 0 respectively.
    /// let flags = Flags::empty();
    /// assert_eq!(flags.lahf(), 0b0000_0010);
    ///
    /// // Force set bits 1, 3 and 5 to test that the correct bits are set.
    /// let flags = Flags::from_bits_retain(0b1111_1101_u8 as u32);
    /// assert_eq!(flags.lahf(), 0b1101_0111);
    /// ```
    pub const fn lahf(self) -> u8 {
        let mut raw = (self.bits() & 0x0000_00FF) as u8;
        // RFLAGS(SF:ZF:0:AF:0:PF:1:CF);
        raw &= !0b0010_1000;
        raw |= 0b0000_0010;
        raw
    }

    /// Stores the value of AH into flags per the SAHF instruction.
    pub const fn with_sahf(self, value: u8) -> Self {
        // Remove undefined bits
        let value = Flags::from_bits_truncate(value as u32);
        let flags = self.union(value);
        // Set bit 1
        let mut raw = flags.bits() | 0b0000_0010;
        // Clear bit 3 and 5.
        raw &= !(0b0010_1000);

        Flags::from_bits_retain(raw)
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub enum Scale {
    _1,
    _2,
    _4,
    _8,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum Memory {
    // 16-bit
    Si(i16),
    Di(i16),
    Bp(i16),
    Bx(i16),
    BxSi(i16),
    BxDi(i16),
    BpSi(i16),
    BpDi(i16),
    Displacement16(i16),

    // 32-bit
    Base {
        base: Reg32,
        displacement: i32,
    },
    BaseIndexScale {
        base: Reg32,
        scale: Scale,
        index: Reg32,
        displacement: i32,
    },
    Displacement32(i32),
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum ControlReg {
    Cr0,
    Cr1,
    Cr2,
    Cr3,
    Cr4,
    Cr5,
    Cr6,
    Cr7,
}

impl ControlReg {
    pub const fn reserved(self) -> bool {
        matches!(self, Self::Cr1 | Self::Cr5 | Self::Cr6 | Self::Cr7)
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum DebugReg {
    Dr0,
    Dr1,
    Dr2,
    Dr3,
    Dr4, // TODO: Reserved sometimes? (CR4.DE)
    Dr5, // TODO: Reserved sometimes? (CR4.DE)
    Dr6,
    Dr7,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum TestReg {
    Tr0, // TODO: These are unused on Intel
    Tr1, // TODO: These are unused on Intel. Also an undocumented test register for 386
    Tr2, // TODO: These are unused on Intel. Also an undocumented test register for 386
    Tr3,
    Tr4,
    Tr5,
    Tr6,
    Tr7,
}

/// Conditional test
#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub enum Cond {
    /// Overflow
    O,

    /// No overflow
    No,

    /// Below, Not above or equal
    B,

    /// Not below, Above or equal
    Nb,

    /// Equal, Zero
    Z,

    /// Not equal, Not zero
    Nz,

    /// Equal
    E,

    /// Not equal
    Ne,

    /// Below or equal, Not above
    Be,

    /// Not below or equal, Above
    Nbe,

    /// Sign
    S,

    /// Not sign
    Ns,

    /// Parity (Even)
    P,

    /// Not parity (Odd)
    Np,

    /// Less than, Not greater than or equal to
    L,

    /// Not less than, Greater than or equal to
    Nl,

    /// Less than or equal to, Not greater than
    Le,

    /// Not less than to equal to, Greater than
    Nle,
}

#[derive(Debug, Default, Clone, Copy, PartialEq, Eq)]
pub enum Operands {
    /// No operands.
    #[default]
    None,

    /// `imm8`
    Immediate8(u8),

    /// `imm16`
    Immediate16(u16),

    /// `imm32`
    Immediate32(u32),

    /// `r8 imm8`
    RegisterImmediate8(Reg8, u8),

    /// `r16 imm16`
    RegisterImmediate16(Reg16, u16),

    /// `r16 imm8`
    ///
    /// Note: This is not sign extended.
    Register16Immediate8(Reg16, u8),

    /// `r32 imm32`
    RegisterImmediate32(Reg32, u32),

    /// `r32 imm8`
    ///
    /// Note: This is not sign extended.
    Register32Immediate8(Reg32, u8),

    /// `m8 imm8`
    MemoryImmediate8(Segment, Memory, u8),

    /// `m16 imm16`
    MemoryImmediate16(Segment, Memory, u16),

    /// `m32 imm32`
    MemoryImmediate32(Segment, Memory, u32),

    /// `r8 m`
    RegisterMemory8(Reg8, Segment, Memory),

    /// `r16 m`
    RegisterMemory16(Reg16, Segment, Memory),

    /// `r32 m`
    RegisterMemory32(Reg32, Segment, Memory),

    /// `m r8`
    MemoryRegister8(Segment, Memory, Reg8),

    /// `m r16`
    MemoryRegister16(Segment, Memory, Reg16),

    /// `m r32`
    MemoryRegister32(Segment, Memory, Reg32),

    /// `segment`
    Segment(Segment),

    /// `rel8` in context of Jcc
    ConditionDisplacement8(Cond, i8),

    /// `rel8` in context of Jcc
    ConditionDisplacement16(Cond, i16),

    /// `rel8` in context of Jcc
    ConditionDisplacement(Cond, i32),

    /// `ptr16:16`
    Pointer16 {
        selector: u16,
        offset: u16,
    },

    /// `ptr16:32`
    Pointer32 {
        selector: u16,
        offset: u32,
    },

    /// `r8`
    Register8(Reg8),

    /// `r16`
    Register16(Reg16),

    /// `r32`
    Register32(Reg32),

    /// `m8`
    Memory8(Segment, Memory),

    /// `m16`
    Memory16(Segment, Memory),

    /// `m32`
    Memory32(Segment, Memory),

    /// `r8 r8`
    RegisterRegister8(Reg8, Reg8),

    /// `r16 r16`
    RegisterRegister16(Reg16, Reg16),

    /// `r32 r32`
    RegisterRegister32(Reg32, Reg32),

    /// `imm16 imm8` in context of `ENTER`.
    Frame(u16, u8),

    /// `r8 CL`
    Register8Cl(Reg8),

    /// `r16 CL`
    Register16Cl(Reg16),

    /// `r32 CL`
    Register32Cl(Reg32),

    /// `m8 CL`
    Memory8Cl(Segment, Memory),

    /// `m16 CL`
    Memory16Cl(Segment, Memory),

    /// `m32 CL`
    Memory32Cl(Segment, Memory),

    /// `r8` in context of `SETcc`
    RegisterSet8(Cond, Reg8),

    /// `r16` in context of `SETcc`
    RegisterSet16(Cond, Reg16),

    /// `r32` in context of `SETcc`
    RegisterSet32(Cond, Reg32),

    /// `m8` in context of `SETcc`
    MemorySet8(Cond, Segment, Memory),

    /// `m16` in context of `SETcc`
    MemorySet16(Cond, Segment, Memory),

    /// `m32` in context of `SETcc`
    MemorySet32(Cond, Segment, Memory),

    /// `r16 r16 imm16`
    RegisterRegisterImmediate16(Reg16, Reg16, u16),

    /// `r32 r32 imm32`
    RegisterRegisterImmediate32(Reg32, Reg32, u32),

    /// `r16 m16 imm16`
    RegisterMemoryImmediate16(Reg16, Segment, Memory, u16),

    /// `r32 m32 imm32`
    RegisterMemoryImmediate32(Reg32, Segment, Memory, u32),

    /// `m8, m8` using DS:(E)SI and ES:(E)DI
    CompareMoveMemory8(Option<RepTy>),

    /// `m16, m16` using DS:(E)SI and ES:(E)DI
    CompareMoveMemory16(Option<RepTy>),

    /// `m32, m32` using DS:(E)SI and ES:(E)DI
    CompareMoveMemory32(Option<RepTy>),

    /// AL destination/source using the port number
    PortAlDx,

    /// AX destination/source using as the port number
    PortAxDx,

    /// EAX destination/source using `dx` as the port number
    PortEaxDx,

    DirectAddress16 {
        segment: u16,
        displacement: u16,
    },
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum Opcode {
    Aaa,
    Aad,
    AadNec,
    Aam,
    AamNec,
    Aas,
    Adc,
    Add,
    And,
    Arpl,
    Bound,
    Call,
    CbwCbwe,
    CwdCdq,
    Cmc,
    Clc,
    Cld,
    Cli,
    Cmp,
    Cmps,
    Daa,
    Das,
    Dec,
    Enter,
    Hlt,
    Imul,
    In,
    Inc,
    Int,
    Int3,
    Into,
    Ins,
    Iret,
    Jcc,
    JcxzJecxz,
    Jecxz,
    Jmp,
    Lahf,
    Lea,
    Leave,
    Lods,
    Loopne,
    Loope,
    Loop,
    Mov,
    Movs,
    Nop,
    Or,
    Out,
    Outs,
    RetFar,
    RetNear,
    Sahf,
    Sbb,
    Scas,
    Stc,
    Std,
    Sti,
    Stos,
    Sub,
    Test,
    Pop,
    Popa,
    Popf,
    Popfd,
    Push,
    Pusha,
    Pushf,
    Pushfd,
    Wait,
    Xchg,
    Xlat,
    Xor,
}

/// An instruction.
#[must_use = "Decoding an instruction can be expensive"]
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct Instruction {
    /// Instruction opcode.
    pub opcode: Opcode,

    /// Instruction operands.
    pub operands: Operands,

    /// The instruction has a lock prefix.
    pub lock: bool,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct Modrm(u8);

impl Modrm {
    pub const fn r#mod(self) -> u8 {
        // overflowing_shr will never fail given the left shift is by 6 on an u8.
        (self.0 & 0b1100_0000).overflowing_shr(6).0
    }

    pub const fn reg(self) -> u8 {
        // overflowing_shr will never fail given the left shift is by 3 on an u8.
        (self.0 & 0b0011_1000).overflowing_shr(3).0
    }

    pub const fn opcode(self) -> u8 {
        self.reg()
    }

    pub const fn rm(self) -> u8 {
        self.0 & 0b0000_0111
    }

    pub const fn is_register_addressing(self) -> bool {
        self.r#mod() == 0b11
    }
}

impl From<u8> for Modrm {
    fn from(value: u8) -> Self {
        Self(value)
    }
}

#[cfg(test)]
mod tests {
    use crate::Flags;

    #[test]
    fn iopl() {
        let empty = Flags::empty();
        assert_eq!(empty.get_iopl(), 0);

        let iopl_0b01 = Flags::IOPL_0;
        assert_eq!(iopl_0b01.get_iopl(), 1);

        let iopl_0b10 = Flags::IOPL_1;
        assert_eq!(iopl_0b10.get_iopl(), 2);

        let iopl_0b11 = Flags::IOPL_0 | Flags::IOPL_1;
        assert_eq!(iopl_0b11.get_iopl(), 3);

        let cf = Flags::CF;
        assert_eq!(cf.get_iopl(), 0);

        let iopl_0b11_nt = Flags::IOPL_0 | Flags::IOPL_1 | Flags::NT;
        assert_eq!(iopl_0b11_nt.get_iopl(), 3);
    }
}
