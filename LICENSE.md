# License

Open98's source code (mostly in the [crates](./crates) directory) is licensed under the [GPL 3.0 license](./LICENSE.GPL3).
