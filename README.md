# A WIP PC-98 emulator 

This is definitely WIP.

## Cloning the repository.

The test submodules are large. I recommend the doing a shallow submodule clone:

```bash
git clone --recurse-submodules --shallow-submodules https://codeberg.org/i509vcb/secretproject.git
```

If you forget to clone the submodules you can run the following:

```bash
git submodule update --init --recursive
```
